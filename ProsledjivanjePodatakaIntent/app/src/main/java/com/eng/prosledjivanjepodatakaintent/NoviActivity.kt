package com.eng.prosledjivanjepodatakaintent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_novi.*

class NoviActivity : AppCompatActivity() {

    var predmetiIzIntenta: ArrayList<Predmet> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_novi)

        predmetiIzIntenta = intent.getSerializableExtra("predmetiList") as ArrayList<Predmet>

        val kreiraniAdapter = MojAdapter(predmetiIzIntenta)
        recyclerViewIntent.adapter = kreiraniAdapter
        recyclerViewIntent.setLayoutManager(GridLayoutManager(this, 1))
    }
}