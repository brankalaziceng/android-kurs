package com.eng.prosledjivanjepodatakaintent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val naziv = intent.getStringExtra("naziv")
        val godinaSlusanja = intent.getStringExtra("godinaSlusanja")
        val semestar = intent.getStringExtra("semestar")
        val espb = intent.getStringExtra("espb")
        val imeProfesora = intent.getStringExtra("imeProfesora")
        val prezimeProfesora = intent.getStringExtra("prezimeProfesora")

        nazivDetail.text = naziv.toString()
        godinaSlusanjaDetail.text = godinaSlusanja.toString()
        semestarDetail.text = semestar.toString()
        espbDetail.text = espb.toString()
        imeProfesoraDetail.text = imeProfesora.toString()
        prezimeProfesoraDetail.text = prezimeProfesora.toString()
    }
}