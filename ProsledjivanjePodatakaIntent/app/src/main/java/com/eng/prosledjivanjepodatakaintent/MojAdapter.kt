package com.eng.prosledjivanjepodatakaintent

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

var rowIndex: Int? = null

class MojAdapter(val preuzetiPodaci: ArrayList<Predmet>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.naziv.text = preuzetiPodaci[position].naziv
        holder.view.godinaSlusanja.text = preuzetiPodaci[position].godinaSlusanja.toString()
        holder.view.semestar.text = preuzetiPodaci[position].semestar
        holder.view.espb.text = preuzetiPodaci[position].espb.toString()
        holder.view.imeProfesora.text = preuzetiPodaci[position].imeProfesora
        holder.view.prezimeProfesora.text = preuzetiPodaci[position].prezimeProfesora

        holder.view.model.setOnClickListener { v ->
            rowIndex = position
            val intent = Intent(v.context, DetailsActivity::class.java)
            intent.putExtra("naziv", preuzetiPodaci[position].naziv)
            intent.putExtra("godinaSlusanja", preuzetiPodaci[position].godinaSlusanja.toString())
            intent.putExtra("semestar", preuzetiPodaci[position].semestar)
            intent.putExtra("espb", preuzetiPodaci[position].espb.toString())
            intent.putExtra("imeProfesora", preuzetiPodaci[position].imeProfesora)
            intent.putExtra("prezimeProfesora", preuzetiPodaci[position].prezimeProfesora)

            v.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}