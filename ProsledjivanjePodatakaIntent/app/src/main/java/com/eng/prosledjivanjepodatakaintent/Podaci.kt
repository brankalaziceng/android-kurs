package com.eng.prosledjivanjepodatakaintent

object  DataGlobal {
    var predmetiDataGlobal: ArrayList<Predmet> = arrayListOf()

    var naziv: String = ""
    var godinaSlusanja: Int = 0
    var semestar: String = ""
    var espb: Int = 0
    var imeProfesora: String = ""
    var prezimeProfesora: String = ""
}