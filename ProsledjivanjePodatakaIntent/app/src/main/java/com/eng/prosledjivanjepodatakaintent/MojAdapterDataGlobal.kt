package com.eng.prosledjivanjepodatakaintent

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

var rowIndexDG: Int? = null

class MojAdapterDataGlobal(val preuzetiPodaci: ArrayList<Predmet>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.naziv.text = preuzetiPodaci[position].naziv
        holder.view.godinaSlusanja.text = preuzetiPodaci[position].godinaSlusanja.toString()
        holder.view.semestar.text = preuzetiPodaci[position].semestar
        holder.view.espb.text = preuzetiPodaci[position].espb.toString()
        holder.view.imeProfesora.text = preuzetiPodaci[position].imeProfesora
        holder.view.prezimeProfesora.text = preuzetiPodaci[position].prezimeProfesora

        holder.view.model.setOnClickListener { v ->
            rowIndex = position
            val intent = Intent(v.context, DetailsDataGlobalActivity::class.java)
            DataGlobal.naziv =  preuzetiPodaci[position].naziv
            DataGlobal.godinaSlusanja =  preuzetiPodaci[position].godinaSlusanja
            DataGlobal.semestar = preuzetiPodaci[position].semestar
            DataGlobal.espb =  preuzetiPodaci[position].espb
            DataGlobal.imeProfesora =  preuzetiPodaci[position].imeProfesora
            DataGlobal.prezimeProfesora = preuzetiPodaci[position].prezimeProfesora

            v.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }
}