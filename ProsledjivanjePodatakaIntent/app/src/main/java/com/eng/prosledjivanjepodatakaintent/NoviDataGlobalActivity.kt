package com.eng.prosledjivanjepodatakaintent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_novi_data_global.*

class NoviDataGlobalActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_novi_data_global)

        val kreiraniAdapter = MojAdapterDataGlobal(DataGlobal.predmetiDataGlobal)
        recyclerViewDataGlobal.adapter = kreiraniAdapter
        recyclerViewDataGlobal.setLayoutManager(GridLayoutManager(this, 1))
    }
}