package com.eng.prosledjivanjepodatakaintent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details_data_global.*

class DetailsDataGlobalActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_data_global)

        nazivDetailDataGlobal.text = DataGlobal.naziv
        godinaSlusanjaDetailDataGlobal.text = DataGlobal.godinaSlusanja.toString()
        semestarDetailDataGlobal.text = DataGlobal.semestar
        espbDetailDataGlobal.text = DataGlobal.espb.toString()
        imeProfesoraDetailDataGlobal.text = DataGlobal.imeProfesora
        prezimeProfesoraDetailDataGlobal.text = DataGlobal.prezimeProfesora
    }
}