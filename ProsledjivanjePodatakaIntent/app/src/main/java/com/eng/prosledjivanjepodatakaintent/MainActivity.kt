package com.eng.prosledjivanjepodatakaintent

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable

data class Predmet(val naziv: String, val godinaSlusanja: Int, val semestar: String,
                   val espb: Int, val imeProfesora: String, val prezimeProfesora: String) : Serializable

class MainActivity : AppCompatActivity() {

    var predmeti: ArrayList<Predmet> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        idiNaListuIntent.setOnClickListener {
            parseCSV()
            val intent = Intent(this, NoviActivity::class.java)
            intent.putExtra("predmetiList", predmeti)
            startActivity(intent)
        }

        idiNaListuDataGlobal.setOnClickListener {
            parseCSVDataGlobal()
            startActivity(Intent(this, NoviDataGlobalActivity::class.java))
        }
    }

    private fun parseCSV() {
        var linija: String?

        val otvoriCSV = InputStreamReader(assets.open("mojiPodaci.csv"))
        val procitajLiniju = BufferedReader(otvoriCSV)

        while(procitajLiniju.readLine().also { linija = it
            } != null) {
            val red: List<String> = linija!!.split(",")

            if(red[0].isEmpty()) Log.d("CSV", "Red je prazan.")

            predmeti.add(Predmet(red[0], red[1].toInt(), red[2], red[3].toInt(), red[4], red[5]))
        }
    }

    private fun parseCSVDataGlobal() {
        var linija: String?

        val otvoriCSV = InputStreamReader(assets.open("mojiPodaci.csv"))
        val procitajLiniju = BufferedReader(otvoriCSV)

        while(procitajLiniju.readLine().also { linija = it
            } != null) {
            val red: List<String> = linija!!.split(",")

            if(red[0].isEmpty()) Log.d("CSV", "Red je prazan.")

            DataGlobal.predmetiDataGlobal.add(Predmet(red[0], red[1].toInt(), red[2], red[3].toInt(), red[4], red[5]))
        }
    }
}