package com.eng.domaci

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min

class MainActivity : AppCompatActivity() {

    val formatOfDate = SimpleDateFormat("dd.MM.YYYY.", Locale.US)
    val formatOfTime = SimpleDateFormat("HH:mm", Locale.US)

    var selectedData = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val startButton = findViewById<Button>(R.id.start)
        startButton.setOnClickListener {
            languagePicker()
        }
    }

    private fun timePicker() {
        val ispis = findViewById<TextView>(R.id.ispisVremena)
        val ispisSvega = findViewById<TextView>(R.id.ispis)
        val currentTime = Calendar.getInstance()

        val timePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { picker, hour_of_day, minute ->
            currentTime.set(Calendar.HOUR_OF_DAY, hour_of_day)
            currentTime.set(Calendar.MINUTE, minute)

            val formatedTime = formatOfTime.format(currentTime.time)
            ispis.text = formatedTime
            selectedData += "\n Time: " + ispis.text.toString()

            ispisSvega.text = selectedData

        }, currentTime.get(Calendar.HOUR_OF_DAY), currentTime.get(Calendar.MINUTE), true)

        timePicker.show()
    }

    private fun datePicker() {
        val ispis = findViewById<TextView>(R.id.ispisDatuma)
        val currentDate = Calendar.getInstance()

        val datePicker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { picker, year, month, dayOfMonth ->
            currentDate.set(Calendar.YEAR, year)
            currentDate.set(Calendar.MONTH, month)
            currentDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val formatedDate = formatOfDate.format(currentDate.time)
            ispis.text = formatedDate
            selectedData += "\n Date: " + ispis.text.toString()
            timePicker()

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH))

        datePicker.show()
    }

    private fun languagePicker() {
        val languages = arrayOf("Srpski", "Ruski", "Engleski", "Njemački", "Španski")
        val ispis = findViewById<TextView>(R.id.ispisJezika)

        val languagePicker = AlertDialog.Builder(this)
        languagePicker.setTitle("Select language")
        languagePicker.setSingleChoiceItems(languages, -1) {picker, index ->
            ispis.text = languages[index]
            selectedData = "Selected data: \n Language: " + ispis.text.toString()
            picker.dismiss()
            datePicker()
        }

        languagePicker.create().show()
    }


}