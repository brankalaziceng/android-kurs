package com.eng.firebasecrud

data class User(val firstName : String? = null,val lastName : String? = null,
                val age : String? = null,val userName : String? = null) {
    constructor() : this("", "", "", "")
}
// val = val arrayOfSubjects : ArrayList<Subjects

data class Subjects(val name : String?, val grade: String?)
