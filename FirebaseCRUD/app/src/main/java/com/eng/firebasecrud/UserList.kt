package com.eng.firebasecrud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*

class UserList : AppCompatActivity() {

    lateinit var database: FirebaseDatabase
    lateinit var reference: DatabaseReference
    lateinit var valueEventListener: ValueEventListener
    lateinit var userRecyclerview : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)

        database = FirebaseDatabase.getInstance()
        reference = database.getReference("Users")

        userRecyclerview = findViewById(R.id.userList)
        userRecyclerview.layoutManager = LinearLayoutManager(this)
        userRecyclerview.setHasFixedSize(true)

        valueEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val userList: ArrayList<User> = arrayListOf()
                for (data in snapshot.children) {
                    val model = data.getValue(User::class.java)
                    userList.add(model as User)
                }
                userRecyclerview.adapter = MyAdapter(userList)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("cancelled", error.toString())
            }
        }

        getAllData()

        findViewById<Button>(R.id.btn_firstname).setOnClickListener {
            reference
                .orderByChild("firstName")
                .startAt(findViewById<EditText>(R.id.searchBar).text.toString())
                .endAt(findViewById<EditText>(R.id.searchBar).text.toString() + "\uf8ff")
                .addListenerForSingleValueEvent(valueEventListener)
        }

        findViewById<Button>(R.id.btn_age).setOnClickListener {
            reference
                .orderByChild("age")
                .startAt(findViewById<EditText>(R.id.searchBar).text.toString())
                .endAt(findViewById<EditText>(R.id.searchBar).text.toString() + "\uf8ff")
                .addListenerForSingleValueEvent(valueEventListener)
        }

        findViewById<Button>(R.id.btn_lastname).setOnClickListener {
            reference
                .orderByChild("lastName")
                .startAt(findViewById<EditText>(R.id.searchBar).text.toString())
                .endAt(findViewById<EditText>(R.id.searchBar).text.toString() + "\uf8ff")
                .addListenerForSingleValueEvent(valueEventListener)
        }
    }

    private fun getAllData() {
        reference.addValueEventListener(valueEventListener)
    }
}