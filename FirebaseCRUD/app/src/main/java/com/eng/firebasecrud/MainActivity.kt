package com.eng.firebasecrud

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.eng.firebasecrud.databinding.ActivityMainBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val goToReadData = findViewById<Button>(R.id.goToReadData)
        goToReadData.setOnClickListener {
            startActivity(Intent(this, ReadData::class.java))
        }

        val goToUpdateData = findViewById<Button>(R.id.goToUpdateData)
        goToUpdateData.setOnClickListener {
            startActivity(Intent(this, UpdateData::class.java))
        }

        val goToDeleteData = findViewById<Button>(R.id.goToDeleteData)
        goToDeleteData.setOnClickListener {
            startActivity(Intent(this, DeleteData::class.java))
        }

        binding.registerBtn.setOnClickListener {

            val firstName = binding.firstName.text.toString()
            val lastName = binding.lastName.text.toString()
            val age = binding.age.text.toString()
            val userName = binding.userName.text.toString()

            database = FirebaseDatabase.getInstance().getReference("Users")
            val User = User(firstName,lastName,age,userName)
            database.child(userName).setValue(User).addOnSuccessListener {

                binding.firstName.text.clear()
                binding.lastName.text.clear()
                binding.age.text.clear()
                binding.userName.text.clear()

                Toast.makeText(this,"Successfully Saved", Toast.LENGTH_SHORT).show()
                Log.d("MESSAGE", "Successfully Saved")

            }.addOnFailureListener{

                Toast.makeText(this,"Failed", Toast.LENGTH_SHORT).show()
                Log.d("MESSAGE", "Failed")
            }
        }
    }
}