package com.eng.firebasecrud

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.eng.firebasecrud.databinding.ActivityDeleteDataBinding
import com.eng.firebasecrud.databinding.ActivityMainBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class DeleteData : AppCompatActivity() {

    private lateinit var binding : ActivityDeleteDataBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeleteDataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val goToMainData = findViewById<Button>(R.id.goToMainData)
        goToMainData.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        val goToUpdateData = findViewById<Button>(R.id.goToUpdateData)
        goToUpdateData.setOnClickListener {
            startActivity(Intent(this, UpdateData::class.java))
        }

        val goToReadData = findViewById<Button>(R.id.goToReadData)
        goToReadData.setOnClickListener {
            startActivity(Intent(this, ReadData::class.java))
        }

        binding.deleteBtn.setOnClickListener {
            var userName = binding.userNameEditText.text.toString()
            if(userName.isNotEmpty()) {
                deleteData(userName)
            } else {
                Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show()
                Log.d("MESSAGE", "Please enter username")
            }
        }
    }

    private fun deleteData(userName: String) {
        database = FirebaseDatabase.getInstance().getReference("Users")
        database.child(userName).removeValue().addOnSuccessListener {

            binding.userNameEditText.text.clear()
            Toast.makeText(this, "Successfuly Deleted", Toast.LENGTH_SHORT).show()
            Log.d("MESSAGE", "Successfuly Deleted")
        }.addOnFailureListener {
            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
            Log.d("MESSAGE", "Failed")
        }
    }
}