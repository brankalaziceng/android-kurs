package com.eng.firebasecrud

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.eng.firebasecrud.databinding.ActivityReadDataBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class ReadData : AppCompatActivity() {

    private lateinit var binding : ActivityReadDataBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityReadDataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val goToMainData = findViewById<Button>(R.id.goToMainData)
        goToMainData.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        val goToUpdateData = findViewById<Button>(R.id.goToUpdateData)
        goToUpdateData.setOnClickListener {
            startActivity(Intent(this, UpdateData::class.java))
        }

        val goToDeleteData = findViewById<Button>(R.id.goToDeleteData)
        goToDeleteData.setOnClickListener {
            startActivity(Intent(this, DeleteData::class.java))
        }

        val goToReadAllData = findViewById<Button>(R.id.goToReadAllData)
        goToReadAllData.setOnClickListener {
            startActivity(Intent(this, UserList::class.java))
        }

        binding.readdataBtn.setOnClickListener {

            val userName : String = binding.etusername.text.toString()
            if  (userName.isNotEmpty()){
                readData(userName)

            }else{
                Toast.makeText(this,"PLease enter the Username",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun readData(userName: String) {

        database = FirebaseDatabase.getInstance().getReference("Users")
        database.child(userName).get().addOnSuccessListener {

            if (it.exists()){
                val firstname = it.child("firstName").value
                val lastName = it.child("lastName").value
                val age = it.child("age").value

                Toast.makeText(this,"Successfuly Read",Toast.LENGTH_SHORT).show()
                Log.d("MESSAGE", "Successfuly Read")

                binding.etusername.text.clear()
                binding.tvFirstName.text = firstname.toString()
                binding.tvLastName.text = lastName.toString()
                binding.tvAge.text = age.toString()

            }else{
                Toast.makeText(this,"User Doesn't Exist",Toast.LENGTH_SHORT).show()
                Log.d("MESSAGE", "User Doesn't Exist")
            }

        }.addOnFailureListener{
            Toast.makeText(this,"Failed",Toast.LENGTH_SHORT).show()
            Log.d("MESSAGE", "Failed")
        }
    }
}