package com.eng.domacizadatak

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dugmeOceneJedan.setOnClickListener {
            ispisOceneJedan.setText("Ocena teksta je: " + ratingBar.rating.toString())
            prikazinaslov.visibility = View.GONE
        }

    }
}