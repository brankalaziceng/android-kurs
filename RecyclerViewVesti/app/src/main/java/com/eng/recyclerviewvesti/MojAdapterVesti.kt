package com.eng.recyclerviewvesti

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

class MojAdapterVesti(val preuzetiPodaci: ArrayList<Vest>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent,false)
        return  CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.titleTextView.text = preuzetiPodaci[position].naslov
        holder.view.subtitleTextView.text = preuzetiPodaci[position].podnaslov
        holder.view.imageView.setImageResource(preuzetiPodaci[position].slika)
    }
}

class CustomViewHolder(val view : View): RecyclerView.ViewHolder(view) {

}