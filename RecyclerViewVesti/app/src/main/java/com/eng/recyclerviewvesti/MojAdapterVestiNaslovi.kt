package com.eng.recyclerviewvesti

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija_naslovi.view.*

class MojAdapterVestiNaslovi(val preuzetiNaslovi: ArrayList<Naslov>, val xmlObjekat: Int) : RecyclerView.Adapter<CustomViewHolder>() {

    var rowIndex = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(xmlObjekat, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.naslov.text = preuzetiNaslovi[position].naslovVesti
        holder.view.imageViewIkonica.setImageResource(preuzetiNaslovi[position].slikaVesti)

        holder.view.setOnClickListener {
            rowIndex = position
            notifyDataSetChanged()
        }
        if (rowIndex == position) {
            holder.view.nasloviLayout.setBackgroundColor(Color.parseColor("#ffe5e5"))
            holder.view.naslov.setTextColor(Color.parseColor("#000000"))
            holder.view.imageViewIkonica.setColorFilter(Color.parseColor("#000000"))
        } else {
            holder.view.nasloviLayout.setBackgroundColor(Color.parseColor("#ff6666"))
            holder.view.naslov.setTextColor(Color.parseColor("#FFFFFF"))
            holder.view.imageViewIkonica.setColorFilter(Color.parseColor("#FFFFFF"))
        }

    }

    override fun getItemCount(): Int {
        return preuzetiNaslovi.count()
    }

}