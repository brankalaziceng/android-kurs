package com.eng.recyclerviewvesti

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

data class Vest(val naslov: String, val podnaslov: String, val slika: Int)

data class Naslov(val naslovVesti: String, val slikaVesti: Int)

class MainActivity : AppCompatActivity() {

    val objectArray: ArrayList<Vest> = arrayListOf()
    val naslovi: ArrayList<Naslov> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createTitleSource()

        val kreiranAdapterDonji = MojAdapterVestiNaslovi(naslovi, R.layout.moja_celija_naslovi)
        recyclerViewBottom.adapter = kreiranAdapterDonji
        recyclerViewBottom.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false))

        createDataSource()

        val kreiranAdapter = MojAdapterVesti(objectArray)
        recyclerView.adapter = kreiranAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))

        val kreiranAdapterGornji = MojAdapterVestiNaslovi(naslovi, R.layout.moja_celija_gornji)
        recyclerViewTop.adapter = kreiranAdapterGornji
        recyclerViewTop.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false))

    }

    private fun createDataSource() {
        objectArray.add(Vest("LIFESTYLE", "Ultimativni kralj među kolačima: Čokoladni sufle je senzacija za sva čula!", R.drawable.prva_vest))
        objectArray.add(Vest("MOJ LJUBIMAC", "Ovo su pet rasa psa koji imaju najslađe štence, ne možete da im odolite.", R.drawable.druga_vest))
        objectArray.add(Vest("TEHNOLOGIJA", "Huawei P50 se pojavio na fotografijama uživo, otklonjene sve dileme.", R.drawable.treca_vest))
        objectArray.add(Vest("LIFESTYLE", "Ultimativni kralj među kolačima: Čokoladni sufle je senzacija za sva čula!", R.drawable.prva_vest))
        objectArray.add(Vest("MOJ LJUBIMAC", "Ovo su pet rasa psa koji imaju najslađe štence, ne možete da im odolite.", R.drawable.druga_vest))
        objectArray.add(Vest("TEHNOLOGIJA", "Huawei P50 se pojavio na fotografijama uživo, otklonjene sve dileme.", R.drawable.treca_vest))
        objectArray.add(Vest("LIFESTYLE", "Ultimativni kralj među kolačima: Čokoladni sufle je senzacija za sva čula!", R.drawable.prva_vest))
        objectArray.add(Vest("MOJ LJUBIMAC", "Ovo su pet rasa psa koji imaju najslađe štence, ne možete da im odolite.", R.drawable.druga_vest))
        objectArray.add(Vest("TEHNOLOGIJA", "Huawei P50 se pojavio na fotografijama uživo, otklonjene sve dileme.", R.drawable.treca_vest))
    }

    fun createTitleSource() {
        naslovi.add(Naslov("Kuhinja", R.drawable.food_icon))
        naslovi.add(Naslov("Sport", R.drawable.sport_icon))
        naslovi.add(Naslov("Tehnologija", R.drawable.technology))
        naslovi.add(Naslov("Moj ljubimac", R.drawable.pet_icon))
        naslovi.add(Naslov("Lifestyle", R.drawable.lifestyle))
    }
}