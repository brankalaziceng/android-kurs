package com.eng.parsiranjeinternet

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import org.json.JSONArray
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://evolutiondoo.com/localJSON.json"

        val slikaURL = "https://assets.entrepreneur.com/content/3x2/2000/20200429211042-GettyImages-1164615296.jpeg?width=700&crop=2:1"

        preuzmiJSON().execute(jsonURL)

        val okvir = findViewById<ImageView>(R.id.slika)

        preuzmiSliku(okvir).execute(slikaURL)
    }

    inner class preuzmiSliku(val okvirZaSliku: ImageView) : AsyncTask<String, Void, Bitmap?>() {

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val urlZaPreuzimanje = params[0]

            for(i in params) {
                println(i)
            }

            println(params.toString())

            try {
                val slikaSaInterneta = URL(urlZaPreuzimanje).openStream()
                image = BitmapFactory.decodeStream(slikaSaInterneta)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            okvirZaSliku.setImageBitmap(result)
        }
    }

    inner class preuzmiJSON : AsyncTask<String, String, String>() {

        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            ispisiPodatke(result)

            println(result)

        }
    }

    fun ispisiPodatke(preuzetiJson: String?) {
        val jsonniz = JSONArray(preuzetiJson)

        var ovoCuIspisati = ""

        jsonniz.let {
            (0 until it.length()).forEach {
                val jsonObjekat = jsonniz.getJSONObject(it)

                val ime = jsonObjekat.getString("ime")
                val prezime = jsonObjekat.getString("prezime")
                val mesto = jsonObjekat.getString("mesto")
                val godiste = jsonObjekat.getInt("godiste")
                val visina = jsonObjekat.getInt("visina")
                val prosek = jsonObjekat.getDouble("prosek")

                ovoCuIspisati = ovoCuIspisati + ime + " " + prezime + " " + mesto + " " + godiste.toString() + " " + visina.toString() + " " + prosek.toString() + "\n"
            }
        }

        findViewById<TextView>(R.id.ispis).text = ovoCuIspisati
    }
}