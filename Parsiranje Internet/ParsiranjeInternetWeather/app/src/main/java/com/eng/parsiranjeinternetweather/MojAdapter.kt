package com.eng.parsiranjeinternetweather

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MojAdapter(val preuzetiPodaci: ArrayList<DataGlobal.WeatherAttributes>) :
    RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        val dateTime = holder.view.findViewById<TextView>(R.id.dateTimeTextView)

        dateTime.text = "Date: " + preuzetiPodaci[position].dt_txt.substring(
            0,
            10
        ) + ", Time: " + preuzetiPodaci[position].dt_txt.substring(11)

        val temp = holder.view.findViewById<TextView>(R.id.tempTextView)
        temp.text = preuzetiPodaci[position].temp.toString()

        val pressure = holder.view.findViewById<TextView>(R.id.pressuretextView)
        pressure.text = "Pressure: " + preuzetiPodaci[position].pressure

        val humidity = holder.view.findViewById<TextView>(R.id.humidityTextView)
        humidity.text = "Humidity: " + preuzetiPodaci[position].humidity + " %"

        val seaLevel = holder.view.findViewById<TextView>(R.id.seaLeveltextView)
        seaLevel.text = "Sea level: " + preuzetiPodaci[position].sea_level

        val grndLevel = holder.view.findViewById<TextView>(R.id.grndLeveltextView)
        grndLevel.text = "Ground level: " + preuzetiPodaci[position].grnd_level

        val tempKf = holder.view.findViewById<TextView>(R.id.tempKftextView)
        tempKf.text = "Temp kf: " + preuzetiPodaci[position].temp_kf

        val windSpeed = holder.view.findViewById<TextView>(R.id.windSpeedTextView)
        windSpeed.text = "Wind speed: " + preuzetiPodaci[position].wind_speed

        val windDeg = holder.view.findViewById<TextView>(R.id.windDegTextView)
        windDeg.text = "Wind deg: " + preuzetiPodaci[position].wind_deg

        val windGust = holder.view.findViewById<TextView>(R.id.windGustTextView)
        windGust.text = "Wind gust: " + preuzetiPodaci[position].wind_gust

        val visibility = holder.view.findViewById<TextView>(R.id.visibilityTextView)
        visibility.text = "Visibility: " + preuzetiPodaci[position].visibility

        val clouds = holder.view.findViewById<TextView>(R.id.cloudsTextView)
        clouds.text = "Clouds: " + preuzetiPodaci[position].clouds

        val pop = holder.view.findViewById<TextView>(R.id.popTextView)
        pop.text = "Pop: " + preuzetiPodaci[position].pop

        val pod = holder.view.findViewById<TextView>(R.id.podTextView)
        pod.text = "Pod: " + preuzetiPodaci[position].sys

        val dt = holder.view.findViewById<TextView>(R.id.dtTextView)
        dt.text = "Dt: " + preuzetiPodaci[position].dt

    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}