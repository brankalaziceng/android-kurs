package com.eng.parsiranjeinternetweather

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        var grad = "London"

        val jsonURL = "https://api.openweathermap.org/data/2.5/forecast?q="+grad+"&appid=356292f8304f3c240d87417fc743ddb0"
        preuzmiJSON().execute(jsonURL)

    }

    inner class preuzmiJSON(): AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            val json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            ispisiPodatke(result)
            startActivity()
            println(result)

        }
    }

    fun ispisiPodatke(preuzetiJSON: String?) {
        val jsonObject = JSONObject(preuzetiJSON)

        val cod = jsonObject.getString("cod")
        val message = jsonObject.getInt("message")
        val cnt = jsonObject.getInt("cnt")

        val list = jsonObject.getJSONArray("list")
        list.let {
            (0 until it.length()).forEach {
                val listObject = list.getJSONObject(it)

                val dt = listObject.getInt("dt")

                val main = listObject.getJSONObject("main")
                val temp = main.getDouble("temp")
                val feels_like = main.getDouble("feels_like")
                val temp_min = main.getDouble("temp_min")
                val temp_max = main.getDouble("temp_max")
                val pressure = main.getInt("pressure")
                val sea_level = main.getInt("sea_level")
                val grnd_level = main.getInt("grnd_level")
                val humidity = main.getInt("humidity")
                val temp_kf = main.getDouble("temp_kf")

                val weather = listObject.getJSONArray("weather")
                var id = 0
                var mainW = ""
                var description = ""
                var icon = ""
                weather.let {
                    (0 until it.length()).forEach {
                        val weatherObject = weather.getJSONObject(it)

                        id = weatherObject.getInt("id")
                        mainW = weatherObject.getString("main")
                        description = weatherObject.getString("description")
                        icon = weatherObject.getString("icon")
                    }
                }

                val clouds = listObject.getJSONObject("clouds")
                val all = clouds.getInt("all")

                val wind = listObject.getJSONObject("wind")
                val speed = wind.getDouble("speed")
                val deg = wind.getInt("deg")
                val gust = wind.getDouble("gust")

                val visibility = listObject.getInt("visibility")
                val pop = listObject.getInt("pop")

                val sys = listObject.getJSONObject("sys")
                val pod = sys.getString("pod")

                val dt_txt = listObject.getString("dt_txt")

                DataGlobal.weatherListArray.add(
                    DataGlobal.WeatherAttributes(dt, temp, feels_like, temp_min, temp_max, pressure, sea_level, grnd_level,
                        humidity, temp_kf, description, all, speed, deg, gust, visibility, pop, pod, dt_txt))
            }
        }

        val city = jsonObject.getJSONObject("city")
        val idC = city.getInt("id")
        val name = city.getString("name")

        val coord = city.getJSONObject("coord")
        val lat = coord.getDouble("lat")
        val lon = coord.getDouble("lon")

        val country = city.getString("country")
        val population = city.getInt("population")
        val timezone = city.getInt("timezone")
        val sunrise = city.getInt("sunrise")
        val sunset = city.getInt("sunset")

        DataGlobal.weatherDataCity = DataGlobal.Weather(cod, message, cnt, DataGlobal.weatherListArray, name, lat, lon, country, population, timezone, sunrise, sunset)
    }

    fun startActivity() {
        startActivity(Intent(this, NewActivity::class.java))
    }
}