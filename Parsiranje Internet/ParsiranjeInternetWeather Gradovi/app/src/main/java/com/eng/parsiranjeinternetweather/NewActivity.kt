package com.eng.parsiranjeinternetweather

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList

data class Gradovi(val naziv: String)

class NewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)

        val cityName = findViewById<TextView>(R.id.nameTextView)
        cityName.text =
            DataGlobal.weatherDataCity.city_name + ", " + DataGlobal.weatherDataCity.country

        val latLon = findViewById<TextView>(R.id.latLonTextView)
        latLon.text =
            "Lat: " + DataGlobal.weatherDataCity.lat + " / " + "Lon: " + DataGlobal.weatherDataCity.lon

        val timezone = findViewById<TextView>(R.id.timezoneTextView)
        timezone.text = "Timezone: " + DataGlobal.weatherDataCity.timezone

        val population = findViewById<TextView>(R.id.populationTextView)
        population.text = "Population: " + DataGlobal.weatherDataCity.population

        val sunrise = findViewById<TextView>(R.id.sunriseTextView)
        sunrise.text = "Sunrise: " + DataGlobal.weatherDataCity.sunrise + " AM"

        val sunset = findViewById<TextView>(R.id.sunsetTextView)
        sunset.text = "Sunset: " + DataGlobal.weatherDataCity.sunset + " PM"

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = MojAdapter(DataGlobal.weatherListArray)
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))

        val danJedan = findViewById<Button>(R.id.danJedan)
        val danDva = findViewById<Button>(R.id.danDva)
        val danTri = findViewById<Button>(R.id.danTri)
        val danCetiri = findViewById<Button>(R.id.danCetiri)
        val danPet = findViewById<Button>(R.id.danPet)

        //datum2, datum3...

        danJedan.setOnClickListener {
            val datum1 = "2021-06-09"

            for (i in DataGlobal.weatherListArray) {
                if (i.dt_txt.contains(datum1)) {
                    DataGlobal.filteredWeatherListArray.add(i)
                }

                recyclerView.adapter = MojAdapter(DataGlobal.filteredWeatherListArray)
                recyclerView.adapter!!.notifyDataSetChanged()
            }
        }

        danDva.setOnClickListener{
            val datum1 = "2021-06-10"

            DataGlobal.filteredWeatherListArray.clear()
            for (i in DataGlobal.weatherListArray) {
                if (i.dt_txt.contains(datum1)) {
                    DataGlobal.filteredWeatherListArray.add(i)
                }

                recyclerView.adapter = MojAdapter(DataGlobal.filteredWeatherListArray)
                recyclerView.adapter!!.notifyDataSetChanged()
            }
        }

        danTri.setOnClickListener{
            val datum1 = "2021-06-11"

            DataGlobal.filteredWeatherListArray.clear()
            for (i in DataGlobal.weatherListArray) {
                if (i.dt_txt.contains(datum1)) {
                    DataGlobal.filteredWeatherListArray.add(i)
                }

                recyclerView.adapter = MojAdapter(DataGlobal.filteredWeatherListArray)
                recyclerView.adapter!!.notifyDataSetChanged()
            }
        }

        danCetiri.setOnClickListener{
            val datum1 = "2021-06-12"

            DataGlobal.filteredWeatherListArray.clear()
            for (i in DataGlobal.weatherListArray) {
                if (i.dt_txt.contains(datum1)) {
                    DataGlobal.filteredWeatherListArray.add(i)
                }

                recyclerView.adapter = MojAdapter(DataGlobal.filteredWeatherListArray)
                recyclerView.adapter!!.notifyDataSetChanged()
            }
        }

        danPet.setOnClickListener{
            val datum1 = "2021-06-13"

            DataGlobal.filteredWeatherListArray.clear()
            for (i in DataGlobal.weatherListArray) {
                if (i.dt_txt.contains(datum1)) {
                    DataGlobal.filteredWeatherListArray.add(i)
                }

                recyclerView.adapter = MojAdapter(DataGlobal.filteredWeatherListArray)
                recyclerView.adapter!!.notifyDataSetChanged()
            }
        }

    }

}