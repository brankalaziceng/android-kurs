package com.eng.parsiranjeinternetapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.abs

class MojAdapter(val preuzetiPodaci: ArrayList<DataGlobal.Docs>) :
    RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val journal = holder.view.findViewById<TextView>(R.id.journalTextView)
        journal.text = preuzetiPodaci[position].journal

        val publication_date = holder.view.findViewById<TextView>(R.id.publicationDateTextView)
        publication_date.text = preuzetiPodaci[position].publication_date.substring(0, 10)

        val eissn = holder.view.findViewById<TextView>(R.id.eissnTextView)
        eissn.text = "e-ISSN: " + preuzetiPodaci[position].eissn

        val article_type = holder.view.findViewById<TextView>(R.id.articleTypeTextView)
        article_type.text = "Article type: " + preuzetiPodaci[position].article_type

        val author_display = holder.view.findViewById<TextView>(R.id.authorDisplayTextView)
        var authors = ""
        for(i in 0 until preuzetiPodaci[position].author_display.length()) {
            authors += preuzetiPodaci[position].author_display[i].toString() + "\n"
        }
        author_display.text = "Authors: " + authors

        val abstract = holder.view.findViewById<TextView>(R.id.abstractTextView)
        var ispis = ""
        for(i in 0 until preuzetiPodaci[position].abstract.length()) {
            ispis += preuzetiPodaci[position].abstract[i].toString() + "\n"
        }
        abstract.text = ispis

        val title_display = holder.view.findViewById<TextView>(R.id.titleDisplayTextView)
        title_display.text = "Title: " + preuzetiPodaci[position].title_display

        val score = holder.view.findViewById<TextView>(R.id.scoreTextView)
        score.text = "Score: " + preuzetiPodaci[position].score
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}