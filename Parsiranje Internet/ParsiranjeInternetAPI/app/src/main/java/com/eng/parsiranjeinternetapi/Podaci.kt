package com.eng.parsiranjeinternetapi

import org.json.JSONArray

object DataGlobal {

    data class Response(
        val numFound: Int,
        val start: Int,
        val maxScore: Double,
        val docs: ArrayList<Docs> = arrayListOf()
    )
    val listOfDocs: ArrayList<Docs> = arrayListOf()

    data class Docs(
        val id: String,
        val journal: String,
        val eissn: String,
        val publication_date: String,
        val article_type: String,
        val author_display: JSONArray,
        val abstract: JSONArray,
        val title_display: String,
        val score: Double
    )

    lateinit var ResponseData: Response
}