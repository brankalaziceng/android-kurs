package com.eng.parsiranjeinternetapi

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val jsonURL = "https://api.plos.org/search?q=title:RNA"
        preuzmiJSON().execute(jsonURL)
    }
    inner class preuzmiJSON : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            inputStreamReader -> inputStreamReader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            ispisPodataka(result)
            startActivity()
            println(result)
        }
    }
    fun ispisPodataka(preuzetiJSON: String?){
        val jsonObject = JSONObject(preuzetiJSON)

        var ovoCuIspisati = ""

        val response = jsonObject.getJSONObject("response")
        val numFound = response.getInt("numFound")
        val start = response.getInt("start")
        val maxScore = response.getDouble("maxScore")
        val docs = response.getJSONArray("docs")

        docs.let {
            (0 until it.length()).forEach {
                val listObject = docs.getJSONObject(it)
                val id = listObject.getString("id")
                val journal = listObject.getString("journal")
                val eissn = listObject.getString("eissn")
                val publication_date = listObject.getString("publication_date")
                val article_type = listObject.getString("article_type")

                val author_display = listObject.getJSONArray("author_display")

                val abstract = listObject.getJSONArray("abstract")
                val title_display = listObject.getString("title_display")
                val score = listObject.getDouble("score")

                DataGlobal.listOfDocs.add(
                    DataGlobal.Docs(id, journal, eissn, publication_date, article_type, author_display, abstract, title_display, score)
                )
            }
        }
        DataGlobal.ResponseData = DataGlobal.Response(numFound, start, maxScore, DataGlobal.listOfDocs)

//        ovoCuIspisati = ovoCuIspisati + maxScore
//        findViewById<TextView>(R.id.ispis).text = ovoCuIspisati
    }
    fun startActivity(){
        startActivity(Intent(this, NewActivity::class.java))
    }
}