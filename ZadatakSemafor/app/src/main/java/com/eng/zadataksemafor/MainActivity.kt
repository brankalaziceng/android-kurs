package com.eng.zadataksemafor

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    var num = 1

//    var isRed: Boolean = true
//    var isYellow: Boolean = false
//    var isGreen: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        promeniBoju()
    }

    fun promeniBoju() {
        var crveno = findViewById<TextView>(R.id.crveno)
        var zuto = findViewById<TextView>(R.id.zuto)
        var zeleno = findViewById<TextView>(R.id.zeleno)

        if(num == 1) {
            crveno.setBackgroundColor(Color.parseColor("#B81D13"))
            zuto.setBackgroundColor(Color.parseColor("#FFFFFF"))
            zeleno.setBackgroundColor(Color.parseColor("#FFFFFF"))
        } else if(num == 2) {
            crveno.setBackgroundColor(Color.parseColor("#B81D13"))
            zuto.setBackgroundColor(Color.parseColor("#EFB700"))
            zeleno.setBackgroundColor(Color.parseColor("#FFFFFF"))
        } else if(num == 3) {
            crveno.setBackgroundColor(Color.parseColor("#FFFFFF"))
            zuto.setBackgroundColor(Color.parseColor("#FFFFFF"))
            zeleno.setBackgroundColor(Color.parseColor("#008450"))
        } else if(num == 4) {
            crveno.setBackgroundColor(Color.parseColor("#FFFFFF"))
            zuto.setBackgroundColor(Color.parseColor("#EFB700"))
            zeleno.setBackgroundColor(Color.parseColor("#FFFFFF"))
        }

        num++
        if(num > 4) {
            num = 1
        }
    }

    fun onTap(view: View) {
        promeniBoju()
    }
}