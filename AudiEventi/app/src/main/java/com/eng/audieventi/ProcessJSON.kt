package com.eng.audieventi

import android.os.AsyncTask
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class ProcessJSON {

    class loadJSON() : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String {
            var json: String
            val connection = URL(p0[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            readDataFromLangsJSON(result)
            println(result)
        }

        fun readDataFromLangsJSON(json: String?) {
            val jsonObject = JSONObject(json)

            for (value in 0 until jsonObject.length()) {
                val nextValue = jsonObject.names()[value].toString()
                DataGlobalJSON.dataFromLangsJSON.put(nextValue, jsonObject.getString(nextValue))
            }
        }

    }
}