package com.eng.audieventi

import android.app.Activity
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity

class BusinessLogic {

    fun concatToRootJSON(value: String): String {
        return DataGlobalJSON.rootJSON + value
    }

    fun setValuesForHomeGuestPage(activity: Activity) {
        val homeGuestEventListTitle = activity.findViewById<TextView>(R.id.homeGuestEventListTitle)
        homeGuestEventListTitle.text = DataGlobalJSON.dataFromLangsJSON.get("HOME_GUEST_EVENT_LIST")

        val homeGuestEventListButton = activity.findViewById<TextView>(R.id.homeGuestEventListButton)
        homeGuestEventListButton.text = DataGlobalJSON.dataFromLangsJSON.get("HOME_GUEST_EVENT_LIST_BUTTON")
    }

}