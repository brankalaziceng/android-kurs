package com.eng.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import kotlinx.android.synthetic.main.activity_main.*

@Suppress("DEPRECATION")
class MainActivity() : AppCompatActivity() {

    private lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DataGlobalJSON.businessLogicObject.setValuesForHomeGuestPage(this)

//        ProcessJSON.loadJSON().execute(DataGlobalJSON.businessLogicObject.concatJSONtoRoot(DataGlobalJSON.langsJSON))

        TestData().populateTestData()

        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navMenuIcon.setOnClickListener {
            drawerLayout.openDrawer(navView)
        }

        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> drawerLayout.closeDrawer(navView)
            }
            true
        }

        navViewFooter.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.impostazioni -> startActivity(Intent(this, SettingsActivity::class.java))
            }
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}