package com.eng.audieventi

object DataGlobalJSON {

    val businessLogicObject = BusinessLogic()

    val rootJSON = "http://mobileapp-coll.engds.it/AudiEventi/"
    val langsJSON = "langs/it_IT.json"

    val configJSON = "config/config.json"

    val dataFromLangsJSON: HashMap<String, String> = hashMapOf()

}