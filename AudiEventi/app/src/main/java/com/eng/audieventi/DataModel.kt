package com.eng.audieventi

class DataModel {

    // vukovic
    data class CalendarEvent(
        val title: String,
        val description: String,
        val image: String
    ) //napraviti listu eventova
    //strana broj 10 skinuti 5 slika


    // vlada
    data class FoodExperience(
        val id: String,
        val title: String,
        val header: String,
        val subtitleFood: String,
        val imageBackground: String,
        val programExperience: ArrayList<ProgramExperience> = arrayListOf()
    )

    data class Allergen(
        val name: String
    )

    data class ProgramExperience(
        val day: String,
        val start: String,
        val type: String,
        val activity: String,
        val site: String,
        val description: String,
        val food: String,
        val allergens: ArrayList<Allergen> = arrayListOf()
    )

    // branka oce
    data class Places(

        val id: String,
        val image_places: String,
        val title: String,
        val subtitle_places: String,
        val title_places: String,
        val description: String,
        val carousel_places: ArrayList<String> = arrayListOf(),
    )
    //branka isto
    data class AudiDrivingExperience(
        val id: String,
        val image_ade: String,
        val description: String,
        val subtitle_ade: String,
        val main_title: String,
        val title_ade: String,
        val carousel_ade: ArrayList<String> = arrayListOf(),
    )


    // preskocicemo
    data class Coupon(
        val action: String?,
        val coupon: String,
        val eventId: String,
        val status: String,
        val eventStatus: String?,
        val valId: String,
        val result: String,
        val resultCode: String,
        val resultMessage: String,
    )
    // ovde ide i contact  JOVANA TI RADIS OVO
    data class ContactsAndInfo(
        val id: String,
        val title: String,
        val free_text: String,
        val imageBackground: String,
        val contactList: ArrayList<Contact> = arrayListOf()

    )
    //jovana 1.1
    data class Contact(
        val name: String,
        val place: String,
        val street: String,
        val region: String,
        val phone: String,
        val phone_fix: String,
        val email: String
    )
    // jovana
    // lista notifikacija da se napravi
    data class Notifications(
        val date: String,
        val title: String,
        val time: String,
        val description: String

    )
    // NEMANJA SVE OVO NADOLE
    data class Activity(
        val start: String,
        val end: String,
        val activityName: String,

        )

    data class DailyActivity(
        val day: String,
        val activityList: ArrayList<Activity> = arrayListOf()
    )

    // jedan objekat  ,u okviru njega activity i daily activity //json
    data class Program(
        val image: String,
        val title: String,
        val subTitle:String,
        val note:String,
        val description: String,
        val infoUri:String,
        val dailyActivityList: ArrayList<DailyActivity> = arrayListOf()
    )
}