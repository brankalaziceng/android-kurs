package com.eng.imdbclone

import android.app.Activity
import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class FunctionClass {

    fun callAdapter(activity: Activity, context: Context, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?, isLinearLayoutManager: Boolean) {
       // val recyclerView = activity.findViewById<RecyclerView>(recyclerViewId)
        recyclerView.adapter = adapter

        if(isLinearLayoutManager) {
            recyclerView.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))
        } else {
            recyclerView.setLayoutManager(GridLayoutManager(context, 1))
        }

    }
}