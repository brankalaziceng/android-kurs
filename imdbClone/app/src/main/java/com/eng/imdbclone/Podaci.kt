package com.eng.imdbclone

object DataGlobal {
    val movieList: ArrayList<Movie> = arrayListOf()

    val functionClass = FunctionClass()

    val listOfMovies: ArrayList<Banner> = arrayListOf()

    val listOfTopPicks: ArrayList<TopPicks> = arrayListOf()

    val listOfFanFavorites: ArrayList<FanFavorites> = arrayListOf()

    val listOfExploreWhatsStreaming: ArrayList<ExploreWhatsStreaming> = arrayListOf()
}