package com.eng.imdbclone

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation

class SignInFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sign_in, container, false)

        val arrowBack = view.findViewById<ImageView>(R.id.arrowBackIcon)

        arrowBack.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_signInFragment_to_youFragment)
        }

        val intentSignInWithIMDbActivity = Intent(activity, SignInWithIMDbActivity::class.java)
        val imdbSignIn = view.findViewById<RelativeLayout>(R.id.imdbSignIn)
        imdbSignIn.setOnClickListener {
           startActivity(intentSignInWithIMDbActivity)
        }

        val intentCreateNewAccount = Intent(activity, CreateAccountActivity::class.java)
        val createIMDbAccount = view.findViewById<Button>(R.id.createIMDbAccount)
        createIMDbAccount.setOnClickListener {
            startActivity(intentCreateNewAccount)
        }

        return view

    }

}