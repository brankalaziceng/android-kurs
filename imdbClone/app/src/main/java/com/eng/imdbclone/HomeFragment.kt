package com.eng.imdbclone

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.eng.imdbclone.adapter.MyAdapterExploreWhatsStreaming
import com.eng.imdbclone.adapter.MyAdapterFanFavorites
import com.eng.imdbclone.adapter.MyAdapterHome
import com.eng.imdbclone.adapter.MyAdapterTopPicks
import kotlinx.android.synthetic.main.fragment_home.*

data class Banner(
    val title: String, val subtitle: String,
    val largePicture: Int, val smallPicture: Int
)

data class TopPicks(
    val posterPicture: Int, val rate: String, val name: String, val year: String,
    val duration: String,
    val button: String
)

data class FanFavorites(
    val posterPicture: Int, val rate: String, val name: String, val year: String,
    val duration: String,
    val button: String
)

data class ExploreWhatsStreaming(
    val posterPicture: Int, val rate: String, val name: String, val year: String,
    val duration: String,
    val button: String
)

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        createMoviesBanner()

        createTopPicks()

        createFanFavorites()

        createExploreWhatsStreaming()

        // Inflate the layout for this fragment

        return view

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        DataGlobal.functionClass.callAdapter(requireActivity(), requireContext(), recyclerViewHome, MyAdapterHome(DataGlobal.listOfMovies), true)
        DataGlobal.functionClass.callAdapter(requireActivity(), requireContext(), recyclerViewTopPicks, MyAdapterTopPicks(DataGlobal.listOfTopPicks), true)
        DataGlobal.functionClass.callAdapter(requireActivity(), requireContext(), recyclerViewFanFavorites, MyAdapterFanFavorites(DataGlobal.listOfFanFavorites), true)
        DataGlobal.functionClass.callAdapter(requireActivity(), requireContext(), recyclerViewExploreWhatsStreaming, MyAdapterExploreWhatsStreaming(DataGlobal.listOfExploreWhatsStreaming), true)


//        val createdAdapter = MyAdapterHome(DataGlobal.listOfMovies)
//        recyclerViewHome!!.adapter = createdAdapter
//        recyclerViewHome!!.setLayoutManager(
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))
//
//        val createdAdapterTopPicks = MyAdapterTopPicks(DataGlobal.listOfTopPicks)
//        recyclerViewTopPicks!!.adapter = createdAdapterTopPicks
//        recyclerViewTopPicks!!.setLayoutManager(
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))
//
//        val createdAdapterFanFragment = MyAdapterFanFavorites(DataGlobal.listOfFanFavorites)
//        recyclerViewFanFavorites!!.adapter = createdAdapterFanFragment
//        recyclerViewFanFavorites!!.setLayoutManager(
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))
//
//        val createdAdapterExploreWhatsStreaming = MyAdapterExploreWhatsStreaming(DataGlobal.listOfExploreWhatsStreaming)
//        recyclerViewExploreWhatsStreaming!!.adapter = createdAdapterExploreWhatsStreaming
//        recyclerViewExploreWhatsStreaming!!.setLayoutManager(
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))

    }

    private fun createExploreWhatsStreaming() {
        DataGlobal.listOfExploreWhatsStreaming.add(
            ExploreWhatsStreaming(R.drawable.indiana, "8.4", "Indiana Jones",
                "1981", "1h 55m","Watch now")
        )

        DataGlobal.listOfExploreWhatsStreaming.add(ExploreWhatsStreaming(R.drawable.gir_next_door, "6.7", "The Girl next Door",
            "2004", "1h 49m","Watch now"))

        DataGlobal.listOfExploreWhatsStreaming.add(ExploreWhatsStreaming(R.drawable.virgin, "7.1", "40 Year-Old Virgin",
            "2005", "1h 56m","Watch now"))

        DataGlobal.listOfExploreWhatsStreaming.add(ExploreWhatsStreaming(R.drawable.saving_private_ryan, "8.6", "Saving Private Ryan",
                "1998", "2h 49m","Watch now"))
    }

    private fun createFanFavorites() {
        DataGlobal.listOfFanFavorites.add(
            FanFavorites(R.drawable.lucaposter, "7.6", "Luca",
            "2021", "1h 41m","+ Watchlist")
        )

        DataGlobal.listOfFanFavorites.add(FanFavorites(R.drawable.cruella, "7.4", "Cruella",
            "2021", "2h 14m","Showtimes"))

        DataGlobal.listOfFanFavorites.add(FanFavorites(R.drawable.loki, "9.1", "Loki",
            "2021 -", "TV-14","+ Watchlist"))

        DataGlobal.listOfFanFavorites.add(FanFavorites(R.drawable.quiet_place, "7.8", "A Quiet Place Part II",
            "2021", "1h 37m","+ Showtimes"))
    }

    private fun createTopPicks() {
        DataGlobal.listOfTopPicks.add(TopPicks(R.drawable.coda_poster, "8.0", "CODA",
                                    "2021", "1h 51m","+ Watchlist"))

        DataGlobal.listOfTopPicks.add(TopPicks(R.drawable.flee_poster, "8.0", "Luca",
            "2021", "1h 23m","+ Watchlist"))

        DataGlobal.listOfTopPicks.add(TopPicks(R.drawable.on_the_count_of_three, "7.4", "On the Count of Three",
            "2021", "1h 24m","+ Watchlist"))

        DataGlobal.listOfTopPicks.add(TopPicks(R.drawable.in_the_same, "7.8", "In the Same Breath",
            "2021", "1h 35m","+ Watchlist"))
    }

    private fun createMoviesBanner() {
        DataGlobal.listOfMovies.add(
            Banner(
                "IMDbrief",
                "For Multitalents for 2021",
                R.drawable.imdbbrief2,
                R.drawable.imdbrief1
            )
        )
        DataGlobal.listOfMovies.add(
            Banner(
                "The Tommorow War",
                "Final Trailer",
                R.drawable.tommorowwar,
                R.drawable.tomorrow_war_xlg
            )
        )
        DataGlobal.listOfMovies.add(
            Banner(
                "IMDb on the Scene - Interviews",
                "Are Jacob Tremblay and Jack Dylan ...",
                R.drawable.luca,
                R.drawable.lucaposter
            )
        )

        DataGlobal.listOfMovies.add(
            Banner(
                "No Small Parts",
                "The Rise of Charlize Theron",
                R.drawable.charlizetheron,
                R.drawable.postercharlize
            )
        )

    }

}