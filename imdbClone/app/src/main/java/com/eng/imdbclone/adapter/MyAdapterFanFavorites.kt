package com.eng.imdbclone.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eng.imdbclone.FanFavorites
import com.eng.imdbclone.R
import kotlinx.android.synthetic.main.my_cell_top_picks.view.*

class MyAdapterFanFavorites(val data: ArrayList<FanFavorites>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.my_cell_fan_favorites, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.rate.text = data[position].rate
        holder.view.name.text = data[position].name
        holder.view.year.text = data[position].year
//        holder.view.parameter.text = data[position].parameter
        holder.view.duration.text = data[position].duration
        holder.view.button.text = data[position].button
        holder.view.posterPicture.setImageResource(data[position].posterPicture)

    }

    override fun getItemCount(): Int {
        return data.count()
    }

}
