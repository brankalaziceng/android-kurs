package com.eng.imdbclone

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

data class Movie(
    val id: String, val rank: String, val title: String, val fullTitle: String,
    val year: String, var image: Bitmap?, val crew: String, val imDbRating: String,
    val imDbRatingCount: String
)

@Suppress("DEPRECATION")
class SplashScreen : AppCompatActivity() {

    var i = 500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val jsonURL = "https://imdb-api.com/en/API/Top250Movies/k_tndu3gjt"
        loadJson().execute(jsonURL)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler().postDelayed({
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 2000)
    }

    inner class loadJson() : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            val json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            println(result)

        }
    }

    fun getDataFromJSON(json: String?) {
        val jsonObject = JSONObject(json)
        val movies = jsonObject.getJSONArray("items")

        movies.let {
            (0 until it.length()).forEach {
                val movie = movies.getJSONObject(it)

                val id = movie.getString("id")
                val rank = movie.getString("rank")
                val title = movie.getString("title")
                val fullTitle = movie.getString("fullTitle")
                val year = movie.getString("year")
                val imageURL = movie.getString("image")
                val crew = movie.getString("crew")
                val imDbRating = movie.getString("imDbRating")
                val imDbRatingCount = movie.getString("imDbRatingCount")

                val imag: Bitmap? = null
                val movieObject = Movie(
                    id, rank, title, fullTitle, year, imag,
                    crew, imDbRating, imDbRatingCount
                )

                preuzmiSliku(movieObject).execute(imageURL)
            }
        }

        println("GOTOVO")
        luper()
    }

    fun luper() {
        Handler(Looper.getMainLooper()).postDelayed({
            if (DataGlobal.movieList.count() != 250) {
                println(i)
                i += 500
                luper()
            }
        }, 500)
    }

    inner class preuzmiSliku(val movie: Movie) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val urlZaPreuzimanje = params[0]

            try {
                val slikaSaInterneta = URL(urlZaPreuzimanje).openStream()
                image = BitmapFactory.decodeStream(slikaSaInterneta)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            movie.image = result
            DataGlobal.movieList.add(movie)
        }
    }
}