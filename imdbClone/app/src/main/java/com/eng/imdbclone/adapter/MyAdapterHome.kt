package com.eng.imdbclone.adapter

import android.view.View

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eng.imdbclone.Banner
import com.eng.imdbclone.R
import kotlinx.android.synthetic.main.my_cell_home_slider.view.*

class MyAdapterHome(val data: ArrayList<Banner>) : RecyclerView.Adapter<CustomViewHolder>() {

    var rowIndex = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.my_cell_home_slider, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.title.text = data[position].title
        holder.view.subtitle.text = data[position].title
        holder.view.bannerPhoto.setImageResource(data[position].largePicture)
        holder.view.smallPictureNextToBannerPhoto.setImageResource(data[position].smallPicture)

    }

    override fun getItemCount(): Int {
        return data.count()
    }

}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}