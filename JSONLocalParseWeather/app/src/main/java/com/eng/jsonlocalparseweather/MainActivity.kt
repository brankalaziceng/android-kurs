package com.eng.jsonlocalparseweather

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        procitajJSON()
    }

    fun procitajJSON() {
        val json: String?

        try {
            val inputStream: InputStream = assets.open("weather.json")
            json = inputStream.bufferedReader().use {
                it.readText()
            }

            val ispis = findViewById<TextView>(R.id.ispis)
            var ovoCuIspisati = ""

            val weatherObject = JSONObject(json)

            val coord = weatherObject.getJSONObject("coord")
            val lon = coord.getDouble("lon")
            val lat = coord.getDouble("lat")

            val weather = weatherObject.getJSONArray("weather")
            var idW = 0
            var mainW = ""
            var description = ""
            var icon = ""

            weather.let {
                (0 until it.length()).forEach {
                    val readWeather = weather.getJSONObject(it)
                    idW = readWeather.getInt("id")
                    mainW = readWeather.getString("main")
                    description = readWeather.getString("description")
                    icon = readWeather.getString("icon")
                }
            }

            val base = weatherObject.getString("base")

            val main = weatherObject.getJSONObject("main")
            val temp = main.getDouble("temp")
            val feels_like = main.getDouble("feels_like")
            val temp_min = main.getDouble("temp_min")
            val temp_max = main.getDouble("temp_max")
            val pressure = main.getInt("pressure")
            val humidity = main.getInt("humidity")

            val visibility = weatherObject.getInt("visibility")

            val wind = weatherObject.getJSONObject("wind")
            val speed = wind.getDouble("speed")
            val deg = wind.getInt("deg")
            val gust = wind.getDouble("gust")

            val clouds = weatherObject.getJSONObject("clouds")
            val all = clouds.getInt("all")

            val dt = weatherObject.getInt("dt")

            val sys = weatherObject.getJSONObject("sys")
            val type = sys.getInt("type")
            val idS = sys.getInt("id")
            val country = sys.getString("country")
            val sunrise = sys.getInt("sunrise")
            val sunset = sys.getInt("sunset")

            val timeZone = weatherObject.getInt("timezone")
            val id = weatherObject.getInt("id")
            val name = weatherObject.getString("name")
            val cod = weatherObject.getInt("cod")

            ovoCuIspisati = ovoCuIspisati + "Country: " + country + "\n\n" +
                    "City: " + name + "\n\n" +
                    "Timezone: " + timeZone + "\n\n" +
                    "Coord: " + "Lon: " + lon.toString() + " / " + "Lat: " + lat.toString() + "\n\n" +
                    "Weather: " + mainW + "\n" + "Description: " + description + "\n\n" +
                    "Base: " + base + "\n\n" +
                    "Temperature: " + temp + "\n" + "Feels like: " + feels_like + "\n" +
                    "Minimum temperature: " + temp_min + "\n" + "Maximum temperature: " + temp_max + "\n" +
                    "Pressure: " + pressure + "\n" + "Humidity: " + humidity + "\n\n" +
                    "Visibility: " + visibility + "\n\n" +
                    "Wind Speed: " + speed + " / " + "Wind direction: " + deg + "\n\n" +
                    "Sunrise: " + sunrise + " / " + "Sunset: " + sunset + "\n\n" +
                    "Timezone: " + timeZone + "\n\n"

            ispis.text = ovoCuIspisati

        } catch (e: IOException) {
            Log.d("JSON Exception", e.toString())
        }
    }
}

data class Weather(
    val coord: JSONObject,
    val weather: JSONArray,
    val base: String,
    val main: JSONObject,
    val visibility: Int,
    val wind: JSONObject,
    val clouds: JSONObject,
    val dt: Int,
    val sys: JSONObject,
    val timezone: Int,
    val id: Int,
    val name: String,
    val cod: Int
)
