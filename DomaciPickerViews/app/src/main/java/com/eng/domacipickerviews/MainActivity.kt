package com.eng.domacipickerviews

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    val formatDatuma = SimpleDateFormat("dd.MM.YYYY.", Locale.US)
    val formatVremena = SimpleDateFormat("HH:mm", Locale.ITALY)

    var odabraniPodaci = " "

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val pokreni = findViewById<Button>(R.id.dugme)
        pokreni.setOnClickListener {
            showChangeLang()
        }
    }

    private fun uzmiVreme() {
        val ispis = findViewById<TextView>(R.id.ispisVremena)
        val ispisPodataka = findViewById<TextView>(R.id.ispisPodataka)
        val sadasnjeVreme = Calendar.getInstance()

        val pikerVreme = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { picker, hour_of_day, minute ->
                sadasnjeVreme.set(Calendar.HOUR_OF_DAY, hour_of_day)
                sadasnjeVreme.set(Calendar.MINUTE, minute)

                val ispisVremena = formatVremena.format(sadasnjeVreme.time)
                ispis.text = ispisVremena
                odabraniPodaci += "\n Vreme: " + ispis.text.toString()

                ispisPodataka.text = odabraniPodaci
            },
            sadasnjeVreme.get(Calendar.HOUR_OF_DAY),
            sadasnjeVreme.get(Calendar.MINUTE),
            true
        )
        pikerVreme.show()
    }

    private fun uzmiDatum() {
        val ispis = findViewById<TextView>(R.id.ispisDatuma)
        val sadasnjeVreme = Calendar.getInstance()

        val pikerDatum = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { picker, year, month, dayOfMonth ->
                sadasnjeVreme.set(Calendar.YEAR, year)
                sadasnjeVreme.set(Calendar.MONTH, month)
                sadasnjeVreme.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val ispisDatuma = formatDatuma.format(sadasnjeVreme.time)
                ispis.text = ispisDatuma
                odabraniPodaci += "\n Datum: " + ispis.text.toString()
                uzmiVreme()

            },
            sadasnjeVreme.get(Calendar.YEAR),
            sadasnjeVreme.get(Calendar.MONTH),
            sadasnjeVreme.get(Calendar.DAY_OF_MONTH)
        )
        pikerDatum.show()
    }

    private fun showChangeLang() {
        val ispis = findViewById<TextView>(R.id.ispisJezika)
        val nizJezika = arrayOf("Srpski", "Italijanski", "Engleski", "Španski")

        //Pravimo AlertDialog
        val pikerJezici = AlertDialog.Builder(this)
        pikerJezici.setTitle("Izaberite jezik")
        pikerJezici.setSingleChoiceItems(nizJezika, -1) { dialog, index ->
            ispis.text = nizJezika[index]
            odabraniPodaci = "Jezik: " + ispis.text.toString()
            dialog.dismiss()
            uzmiDatum()
        }
        //Uzmi AlertDialog i prikazi ga
        val prikazi = pikerJezici.create()
        prikazi.show()
    }


}