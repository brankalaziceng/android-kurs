package com.eng.katalogautomobiladrugideo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

data class Car(
    val marka: String,
    val model: String,
    val motor: String,
    val paketOpreme: String,
    val cena: String,
    val rasporedCilindara: String,
    val brojVentila: String,
    val precnikHodKlipa: String,
    val tipUbrizgavanja: String,
    val sistemOtvaranjaVentila: String,
    val turbo: String,
    val zapreminaMotora: String,
    val kW: String,
    val kS: String,
    val snagaPriObrtajima: String,
    val obrtniMoment: String,
    val obrtniMomentPriObrtajima: String,
    val stepenKompresije: String,
    val tipMenjaca: String,
    val brojStepeniPrenosa: String,
    val pogon: String,
    val duzina: String,
    val sirina: String,
    val visina: String,
    val medjuosovinskoRastojanje: String,
    val tezinaPraznogVozila: String,
    val maksimalnaDozvoljenaTezina: String,
    val zapreminaRezervoara: String,
    val zapreminaPrtljaznika: String,
    val maksZapreminaPrtljaznika: String,
    val dozvoljenTovar: String,
    val dozvoljenoOpterecenjeKrova: String,
    val dozvoljenaTezinaPrikoliceBK: String,
    val dozvoljenaTezinaPrikoliceSK12: String,
    val dozvoljenaTezinaPrikoliceSK8: String,
    val opterecenjeKuke: String,
    val radijusOkretanja: String,
    val tragTockovaNapred: String,
    val tragTockovaNazad: String,
    val maksimalnaBrzina: String,
    val ubrzanje0_100 : String,
    val ubrzanje0_200: String,
    val ubrzanje80_120FinalniStepen: String,
    val zaustavniPut100: String,
    val vremeZa400m: String,
    val potrosnjaGrad: String,
    val potrosnjaVGrada: String,
    val emisijaCO2: String,
    val katalizator: String,
    val dimenzijePneumatika: String,
    val prednjeOpruge: String,
    val zadnjeOpruge: String,
    val prednjiStabilizator: String,
    val zadnjiStabilizator: String,
    val garancijaKorozija: String,
    val garancijaMotor: String,
    val euroNCAP: String,
    val euroNCAPZvezdice: String,
    val gorivo: String,
    val brojVrata: String,
    val brojSedista: String
)

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val uvezenaKlasa = NovaKlasa()

        uvezenaKlasa.parseCSV(this)

        seeAllModels.setOnClickListener {
            startActivity(Intent(this, LogotipiActivity::class.java))
        }
    }

}