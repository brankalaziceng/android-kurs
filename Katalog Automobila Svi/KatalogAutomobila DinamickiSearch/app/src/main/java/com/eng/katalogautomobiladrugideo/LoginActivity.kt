package com.eng.katalogautomobiladrugideo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

val username = ""
val password = ""

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        prosledi.setOnClickListener {
            val editUsername = usernameEdit.text.toString()
            val editPassword = passwordEdit.text.toString()

            if (username == editUsername && password == editPassword) {
                Log.d("LOGIN", "Korisnik je ulogovan")
                Toast.makeText(applicationContext, "Uspesno ste ulogovani", Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                Log.d("LOGIN", "Korisnik nije ulogovan")
                Toast.makeText(applicationContext, "Podaci nisu ispravni", Toast.LENGTH_SHORT).show()
            }
        }
    }
}