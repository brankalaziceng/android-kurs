package com.eng.katalogautomobiladrugideo

object DataGlobal {

    val uvezenaKlasa = NovaKlasa()

    var cars: ArrayList<Car> = arrayListOf()

    var carModel = ""

    var carLogo: Int = 0

    data class CarImage(val image: Int, val car: String)

    data class OdabraniAutomobili(val slika: Int, val model: String, val cena: String, val marka: String,
                              val motor: String, val paketOpreme: String, val tezinaPraznogVozila: String)

}
