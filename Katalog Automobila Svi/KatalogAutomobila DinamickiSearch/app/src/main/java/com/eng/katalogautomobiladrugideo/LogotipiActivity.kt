package com.eng.katalogautomobiladrugideo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_logotipi.*

var rowIndex: Int? = null

data class Logotipi(val slika: Int, val naziv: String)

class LogotipiActivity : AppCompatActivity() {

    val listaAutomobila: ArrayList<Logotipi> = arrayListOf()

    var listaPretrazenihAutomobila: ArrayList<DataGlobal.OdabraniAutomobili> = arrayListOf()

    val uvezenaKlasa = NovaKlasa()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logotipi)

        uvezenaKlasa.createDataSource(listaAutomobila)

        val kreiraniAdapter = MojAdapterLogotip(listaAutomobila)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))

        val unosTeksta = pretraga

        unosTeksta.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filtrirajModele(s.toString())
            }
        })

        dugmeNazad.setOnClickListener {
            pretraga.setText("")
            val kreiraniAdapter = MojAdapterLogotip(listaAutomobila)
            recyclerView.adapter = kreiraniAdapter
            recyclerView.layoutManager = GridLayoutManager(this, 2)
        }

//        dugmePretraga.setOnClickListener {
//            listaPretrazenihAutomobila.clear()
//
//            for (car in DataGlobal.cars) {
//                //val unos = (car.model).toLowerCase()
//                val unos = (car.marka + " " + car.model).toLowerCase()
//
//                if (pretraga.text.toString().isNotEmpty() && unos.contains(
//                        pretraga.text.toString().toLowerCase())) {
//                    val slika = uvezenaKlasa.getPictureOfACar(car.marka)
//                    listaPretrazenihAutomobila.add(
//                        DataGlobal.OdabraniAutomobili(
//                            uvezenaKlasa.getPictureOfACar(car.marka), car.model, car.cena + " $", car.marka, car.motor,
//                            car.paketOpreme, car.tezinaPraznogVozila))
//                    Log.d("image", car.marka)
//                }
//            }
//            recyclerView.adapter = MojAdapterModel(listaPretrazenihAutomobila)
//            recyclerView.layoutManager = GridLayoutManager(this, 2)
//        }

    }

    fun filtrirajModele(korisnickiUnos: String) {
        listaPretrazenihAutomobila.clear()

        for (car in DataGlobal.cars) {
            //val unos = (car.model).toLowerCase()
            val unos = (car.marka + " " + car.model).toLowerCase()

            if (pretraga.text.toString().isNotEmpty() && unos.contains(
                    korisnickiUnos.toLowerCase())) {
                val slika = uvezenaKlasa.getPictureOfACar(car.marka)
                listaPretrazenihAutomobila.add(
                    DataGlobal.OdabraniAutomobili(
                        uvezenaKlasa.getPictureOfACar(car.marka), car.model, car.cena + " $", car.marka, car.motor,
                        car.paketOpreme, car.tezinaPraznogVozila))
                Log.d("image", car.marka)
            }
        }
        recyclerView.adapter = MojAdapterModel(listaPretrazenihAutomobila)
        recyclerView.layoutManager = GridLayoutManager(this, 2)
    }

}

