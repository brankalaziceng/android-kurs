package com.eng.katalogautomobiladrugideo

import android.content.Context
import java.io.BufferedReader
import java.io.InputStreamReader

class NovaKlasa {

    fun createDataSource(listaAutomobila: ArrayList<Logotipi>) {
        listaAutomobila.clear()
        listaAutomobila.add(Logotipi(R.drawable.alfa_romeo, "Alfa Romeo"))
        listaAutomobila.add(Logotipi(R.drawable.audi, "Audi"))
        listaAutomobila.add(Logotipi(R.drawable.citroen, "Citroen"))
        listaAutomobila.add(Logotipi(R.drawable.dacia, "Dacia"))
        listaAutomobila.add(Logotipi(R.drawable.fiat, "Fiat"))
        listaAutomobila.add(Logotipi(R.drawable.ford, "Ford"))
        listaAutomobila.add(Logotipi(R.drawable.honda, "Honda"))
        listaAutomobila.add(Logotipi(R.drawable.hyndai, "Hyundai"))
        listaAutomobila.add(Logotipi(R.drawable.infiniti, "Infiniti"))
        listaAutomobila.add(Logotipi(R.drawable.isuzu, "Isuzu"))
        listaAutomobila.add(Logotipi(R.drawable.jeep, "Jeep"))
        listaAutomobila.add(Logotipi(R.drawable.lada, "Lada"))
        listaAutomobila.add(Logotipi(R.drawable.mazda, "Mazda"))
        listaAutomobila.add(Logotipi(R.drawable.mercedes_benz, "Mercedes-Benz"))
        listaAutomobila.add(Logotipi(R.drawable.mini, "Mini"))
        listaAutomobila.add(Logotipi(R.drawable.mitsubishi, "Mitsubishi"))
        listaAutomobila.add(Logotipi(R.drawable.nissan, "Nissan"))
        listaAutomobila.add(Logotipi(R.drawable.opel, "Opel"))
        listaAutomobila.add(Logotipi(R.drawable.peugeot, "Peugeot"))
        listaAutomobila.add(Logotipi(R.drawable.renault, "Renault"))
        listaAutomobila.add(Logotipi(R.drawable.seat, "Seat"))
        listaAutomobila.add(Logotipi(R.drawable.skoda, "Skoda"))
        listaAutomobila.add(Logotipi(R.drawable.smart, "Smart"))
        listaAutomobila.add(Logotipi(R.drawable.subaru, "Subaru"))
        listaAutomobila.add(Logotipi(R.drawable.suzuki, "Suzuki"))
        listaAutomobila.add(Logotipi(R.drawable.volkswagen, "Volkswagen"))
        listaAutomobila.add(Logotipi(R.drawable.volvo, "Volvo"))
    }

    fun getPictureOfACar(marka: String): Int {
        if (marka == "Alfa Romeo") {
            return R.drawable.alfa_romeo_giulietta
        } else if (marka.contains("Audi")) {
            return R.drawable.audi_picture
        } else if (marka == "Citroen") {
            return R.drawable.citroen_picture
        } else if (marka == "Dacia") {
            return R.drawable.dacia_white
        } else if (marka == "Fiat") {
            return R.drawable.fiat_500_l
        } else if (marka == "Ford") {
            return R.drawable.ford_fiesta
        } else if (marka == "Honda") {
            return R.drawable.honda_orange
        } else if (marka == "Hyundai") {
            return R.drawable.hyundai_blue
        } else if (marka == "Infiniti") {
            return R.drawable.infiniti_automobil
        } else if (marka == "Isuzu") {
            return R.drawable.isuzu_sivi
        } else if (marka == "Jeep") {
            return R.drawable.jeep_beli
        }
        return R.drawable.automobil
    }

    fun parseCSV(context: Context) {
        var line: String?
        val openCSV = InputStreamReader(context.assets.open("modelExcelNew.csv"))
        val readLineInCSV = BufferedReader(openCSV)
        val row: List<String> = readLineInCSV.readLine()!!.split(";")
        while (readLineInCSV.readLine().also {
                line = it
            } != null) {
            val row: List<String> = line!!.split(";")
            if (row[0].isNotEmpty()) {
                DataGlobal.cars.add(
                    Car(
                        row[0],
                        row[1],
                        row[2],
                        row[3],
                        row[4],
                        row[5],
                        row[6],
                        row[7],
                        row[8],
                        row[9],
                        row[10],
                        row[11],
                        row[12],
                        row[13],
                        row[14],
                        row[15],
                        row[16],
                        row[17],
                        row[18],
                        row[19],
                        row[20],
                        row[21],
                        row[22],
                        row[23],
                        row[24],
                        row[25],
                        row[26],
                        row[27],
                        row[28],
                        row[29],
                        row[30],
                        row[31],
                        row[32],
                        row[33],
                        row[34],
                        row[35],
                        row[36],
                        row[37],
                        row[38],
                        row[39],
                        row[40],
                        row[41],
                        row[42],
                        row[43],
                        row[44],
                        row[45],
                        row[46],
                        row[47],
                        row[48],
                        row[49],
                        row[50],
                        row[51],
                        row[52],
                        row[53],
                        row[54],
                        row[55],
                        row[56],
                        row[57],
                        row[58],
                        row[59],
                        row[60]
                    )
                )
            }
        }
        println(DataGlobal.cars.toString())
    }


}