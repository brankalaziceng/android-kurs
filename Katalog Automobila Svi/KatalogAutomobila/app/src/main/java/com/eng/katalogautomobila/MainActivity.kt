package com.eng.katalogautomobila

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader

data class Car(
    val marka: String,
    val model: String,
    val motor: String,
    val paketOpreme: String,
    val cena: String,
    val rasporedCilindara: String,
    val brojVentila: String,
    val precnikHodKlipa: String,
    val tipUbrizgavanja: String,
    val sistemOtvaranjaVentila: String,
    val turbo: String,
    val zapreminaMotora: String,
    val kW: String,
    val kS: String,
    val snagaPriObrtajima: String,
    val obrtniMoment: String,
    val obrtniMomentPriObrtajima: String,
    val stepenKompresije: String,
    val tipMenjaca: String,
    val brojStepeniPrenosa: String,
    val pogon: String,
    val duzina: String,
    val sirina: String,
    val visina: String,
    val medjuosovinskoRastojanje: String,
    val tezinaPraznogVozila: String,
    val maksimalnaDozvoljenaTezina: String,
    val zapreminaRezervoara: String,
    val zapreminaPrtljaznika: String,
    val maksZapreminaPrtljaznika: String,
    val dozvoljenTovar: String,
    val dozvoljenoOpterecenjeKrova: String,
    val dozvoljenaTezinaPrikoliceBK: String,
    val dozvoljenaTezinaPrikoliceSK12: String,
    val dozvoljenaTezinaPrikoliceSK8: String,
    val opterecenjeKuke: String,
    val radijusOkretanja: String,
    val tragTockovaNapred: String,
    val tragTockovaNazad: String,
    val maksimalnaBrzina: String,
    val ubrzanje0_100 : String,
    val ubrzanje0_200: String,
    val ubrzanje80_120FinalniStepen: String,
    val zaustavniPut100: String,
    val vremeZa400m: String,
    val potrosnjaGrad: String,
    val potrosnjaVGrada: String,
    val emisijaCO2: String,
    val katalizator: String,
    val dimenzijePneumatika: String,
    val prednjeOpruge: String,
    val zadnjeOpruge: String,
    val prednjiStabilizator: String,
    val zadnjiStabilizator: String,
    val garancijaKorozija: String,
    val garancijaMotor: String,
    val euroNCAP: String,
    val euroNCAPZvezdice: String,
    val gorivo: String,
    val brojVrata: String,
    val brojSedista: String
)

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        parseCSV()

        seeAllModels.setOnClickListener {
            startActivity(Intent(this, LogotipiActivity::class.java))
        }
    }

    private fun parseCSV() {
        var line: String?
        val openCSV = InputStreamReader(assets.open("modelExcelNew.csv"))
        val readLineInCSV = BufferedReader(openCSV)
        val row: List<String> = readLineInCSV.readLine()!!.split(";")
        while (readLineInCSV.readLine().also {
                line = it
            } != null) {
            val row: List<String> = line!!.split(";")
            if (row[0].isNotEmpty()) {
                DataGlobal.cars.add(
                    Car(
                        row[0],
                        row[1],
                        row[2],
                        row[3],
                        row[4],
                        row[5],
                        row[6],
                        row[7],
                        row[8],
                        row[9],
                        row[10],
                        row[11],
                        row[12],
                        row[13],
                        row[14],
                        row[15],
                        row[16],
                        row[17],
                        row[18],
                        row[19],
                        row[20],
                        row[21],
                        row[22],
                        row[23],
                        row[24],
                        row[25],
                        row[26],
                        row[27],
                        row[28],
                        row[29],
                        row[30],
                        row[31],
                        row[32],
                        row[33],
                        row[34],
                        row[35],
                        row[36],
                        row[37],
                        row[38],
                        row[39],
                        row[40],
                        row[41],
                        row[42],
                        row[43],
                        row[44],
                        row[45],
                        row[46],
                        row[47],
                        row[48],
                        row[49],
                        row[50],
                        row[51],
                        row[52],
                        row[53],
                        row[54],
                        row[55],
                        row[56],
                        row[57],
                        row[58],
                        row[59],
                        row[60]))
            }
        }
        println(DataGlobal.cars.toString())
    }

}