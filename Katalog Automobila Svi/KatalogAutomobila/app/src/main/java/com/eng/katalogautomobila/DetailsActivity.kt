package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.moja_celija_modeli.*

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        logoZaDetails.setImageResource(DataGlobal.carLogo)

        var open = false

        scroll.setOnClickListener {
            if (!open) {
                ostatakAtributa.visibility = View.VISIBLE
                scroll.text = "Sakrij detaljno"
            } else {
                ostatakAtributa.visibility = View.GONE
                scroll.text = "Prikazi detaljno"
            }
            open = !open
        }

        val slikaDetail = intent.getIntExtra("slika", 0)
        val markaDetail = intent.getStringExtra("marka")
        val modelDetail = intent.getStringExtra("model")
        val motorDetail = intent.getStringExtra("motor")
        val paketOpremeDetail = intent.getStringExtra("paketOpreme")
        val tezinaDetail = intent.getStringExtra("tezina")

        val imageTop = slikaAutomobila
        imageTop.setImageResource(slikaDetail)

        for (car in DataGlobal.cars) {
            if (car.marka == markaDetail && car.model == modelDetail && car.motor == motorDetail &&
                car.paketOpreme == paketOpremeDetail && car.tezinaPraznogVozila == tezinaDetail
            ) {
                val prvihDeset = prvihDeset
                val ostatakAtributa = ostatakAtributa

                prvihDeset.text = "MARKA: " + car.marka + "\nMODEL: " + car.model +
                        "\nMOTOR: " + car.motor + "\nPAKET OPREME: " + car.paketOpreme +
                        "\nCENA: " + car.cena + " $" + "\nRASPORED CILINDARA: " + car.rasporedCilindara +
                        "\nBROJ VENTILA: " + car.brojVentila + "\nPRECNIK KOD KLIPA: " +
                        car.precnikHodKlipa + "\nTIP UBRIZGAVANJA: " + car.tipUbrizgavanja +
                        "\nSISTEM OTVARANJA VENTILA: " + car.sistemOtvaranjaVentila

                ostatakAtributa.text = "TURBO: " + car.turbo + "\nZAPREMINA MOTORA: " + car.zapreminaMotora +
                            "\nKW: " + car.kW + "\nKS: " + car.kS + "\nSNAGA PRI OBRTAJIMA: " + car.snagaPriObrtajima +
                            "\nOBRTNI MOMENT: " + car.obrtniMoment + "\nOBRTNI MOMENT PRI OBRTAJIMA: " +
                            car.obrtniMomentPriObrtajima + "\nSTEPEN KOMPRESIJE: " + car.stepenKompresije +
                            "\nTIP MENJACA: " + car.tipMenjaca + "\nBROJ STEPENI PRENOSA: " + car.brojStepeniPrenosa +
                            "\nPOGON: " + car.pogon + "\nDUZINA: " + car.duzina + "\nSIRINA: " + car.sirina +
                            "\nVISINA: " + car.visina + "\nMEDJUOSOVINSKO RASTOJANJE: " + car.medjuosovinskoRastojanje +
                            "\nTEZINA PRAZNOG VOZILA: " + car.tezinaPraznogVozila + "\nMAKSIMALNA DOZVOLJENA TEZINA: " +
                            car.maksimalnaDozvoljenaTezina + "\nZAPREMINA REZERVOARA: " + car.zapreminaRezervoara +
                            "\nZAPREMINA PRTLJAZNIKA: " + car.zapreminaPrtljaznika + "\nMAKS.ZAPREMINA PRTLJAZNIKA: " +
                            car.maksZapreminaPrtljaznika + "\nDOZVOLJEN TOVAR: " + car.dozvoljenTovar +
                            "\nDOZVOLJENO OPTERECENJE KROVA: " + car.dozvoljenoOpterecenjeKrova +
                            "\nDOZVOLJENA TEZINA PRIKOLICE BK: " + car.dozvoljenaTezinaPrikoliceBK +
                            "\nDOZVOLJENA TEZINA PRIKOLICE SK12: " + car.dozvoljenaTezinaPrikoliceSK12 +
                            "\nDOZVOLJENA TEZINA PRIKOLICE SK8: " + car.dozvoljenaTezinaPrikoliceSK8 +
                            "\nOPRERECENJE KUKE: " + car.opterecenjeKuke + "\nRADIJUS OKRETANJA: " + car.radijusOkretanja +
                            "\nTRAG TOCKOVA NAPRED: " + car.tragTockovaNapred + "\nTRAG TOCKOVA NAZAD: " +
                            car.tragTockovaNazad + "\nMAKSIMALNA BRZINA: " + car.maksimalnaBrzina + "\nUBRZANJE 0-100: " +
                            car.ubrzanje0_100 + "\nUBRZANJE 0-200: " + car.ubrzanje0_200 + "\nUBRZANJE 80-120 FINALNI STEPEN: " +
                            car.ubrzanje80_120FinalniStepen + "\nZAUSTAVNI PUT 100:" + car.zaustavniPut100 + "\nVREME ZA 400m: " +
                            car.vremeZa400m + "\nPOTROSNJA GRAD: " + car.potrosnjaGrad + "\nPOTROSNJA VAN GRADA: " +
                            car.potrosnjaVGrada + "\nEMISIJA CO2: " + car.emisijaCO2 + "\nDIMENZIJE PNEUMATIKA: " +
                            car.dimenzijePneumatika + "\nPREDNJE OPRUGE: " + car.prednjeOpruge + "\nZADNJE OPRUGE: " +
                            car.zadnjeOpruge + "\nPREDNJI STABILIZATOR: " + car.prednjiStabilizator +
                            "\nZADNJI STABILIZATOR: " + car.zadnjiStabilizator + "\nGARANCIJA KOROZIJA: " +
                            car.garancijaKorozija + "\nGARANCIJA MOTOR: " + car.garancijaMotor + "\nEURO NCAP: " +
                            car.euroNCAP + "\nEURO NCAP ZVEZDICE: " + car.euroNCAP + "\nGORIVO: " + car.gorivo +
                            "\nBROJ VRATA: " + car.brojVrata + "\nBROJ SEDISTA: " + car.brojSedista
            }
        }
    }
}