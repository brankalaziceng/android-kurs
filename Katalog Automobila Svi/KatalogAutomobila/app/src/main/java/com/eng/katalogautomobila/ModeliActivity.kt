package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_modeli.*

class ModeliActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modeli)

        logo.setImageResource(DataGlobal.carLogo)

        var filteredCars: ArrayList<DataGlobal.OdabraniAutomobili> = arrayListOf()

        for (car in DataGlobal.cars) {
            if (car.marka == DataGlobal.carModel) {
                filteredCars.add(
                    DataGlobal.OdabraniAutomobili(
                        DataGlobal.getPictureOfACar(car.marka), car.model, car.cena + " $", car.marka,
                        car.motor, car.paketOpreme, car.tezinaPraznogVozila
                    )
                )
            }
        }

        Log.d("LISTA", filteredCars.count().toString())

        val kreiraniAdapter = MojAdapterModel(filteredCars)
        recyclerViewModeli.adapter = kreiraniAdapter
        recyclerViewModeli.setLayoutManager(GridLayoutManager(this, 2))
    }

//    private fun getPictureOfACar(marka: String): Int {
//        if (marka == "Alfa Romeo") {
//            return R.drawable.alfa_romeo_giulietta
//        } else if (marka == "Audi") {
//            return R.drawable.audi_picture
//        } else if (marka == "Citroen") {
//            return R.drawable.citroen_picture
//        } else if (marka == "Dacia") {
//            return R.drawable.dacia_white
//        } else if (marka == "Fiat") {
//            return R.drawable.fiat_500_l
//        } else if (marka == "Ford") {
//            return R.drawable.ford_fiesta
//        } else if (marka == "Honda") {
//            return R.drawable.honda_orange
//        } else if (marka == "Hyundai") {
//            return R.drawable.hyundai_blue
//        } else if (marka == "Infiniti") {
//            return R.drawable.infiniti_automobil
//        } else if (marka == "Isuzu") {
//            return R.drawable.isuzu_sivi
//        } else if (marka == "Jeep") {
//            return R.drawable.jeep_beli
//        }
//        return R.drawable.automobil
//    }
}