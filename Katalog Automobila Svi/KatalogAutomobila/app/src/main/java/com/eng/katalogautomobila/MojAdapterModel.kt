package com.eng.katalogautomobila

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija_modeli.view.*

class MojAdapterModel(val preuzetiPodaci: ArrayList<DataGlobal.OdabraniAutomobili>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija_modeli, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val slika = DataGlobal.getPictureOfACar(preuzetiPodaci[position].marka)
        holder.view.automobilSlika.setImageResource(slika)
        holder.view.nazivModela.text = preuzetiPodaci[position].model
        holder.view.cenaModel.text = preuzetiPodaci[position].cena

        holder.view.modelZaMarke.setOnClickListener { view ->
            rowIndex = position
            val intent = Intent(view.context, DetailsActivity::class.java)
            intent.putExtra("cena", preuzetiPodaci[position].cena)
            intent.putExtra("slika", preuzetiPodaci[position].slika)
            intent.putExtra("marka", preuzetiPodaci[position].marka)
            intent.putExtra("model", preuzetiPodaci[position].model)
            intent.putExtra("motor", preuzetiPodaci[position].motor)
            intent.putExtra("paketOpreme", preuzetiPodaci[position].paketOpreme)
            intent.putExtra("tezina", preuzetiPodaci[position].tezinaPraznogVozila)
            view.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

}
