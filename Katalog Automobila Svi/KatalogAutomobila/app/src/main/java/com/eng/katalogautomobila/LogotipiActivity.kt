package com.eng.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_logotipi.*

var rowIndex: Int? = null

data class Logotipi(val slika: Int, val naziv: String)

class LogotipiActivity : AppCompatActivity() {

    val listaAutomobila: ArrayList<Logotipi> = arrayListOf()

    var listaPretrazenihAutomobila: ArrayList<DataGlobal.OdabraniAutomobili> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logotipi)

        createDataSource()

        val kreiraniAdapter = MojAdapterLogotip(listaAutomobila)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))

        dugmePretraga.setOnClickListener {
            listaPretrazenihAutomobila.clear()

            for (car in DataGlobal.cars) {
                //val unos = (car.model).toLowerCase()
                val unos = (car.marka + " " + car.model).toLowerCase()

                if (pretraga.text.toString().isNotEmpty() && unos.contains(
                        pretraga.text.toString().toLowerCase())) {
                    val slika = DataGlobal.getPictureOfACar(car.marka)
                    listaPretrazenihAutomobila.add(
                        DataGlobal.OdabraniAutomobili(
                            DataGlobal.getPictureOfACar(car.marka), car.model, car.cena + " $", car.marka, car.motor,
                            car.paketOpreme, car.tezinaPraznogVozila))
                    Log.d("image", car.marka)
                }
            }
            recyclerView.adapter = MojAdapterModel(listaPretrazenihAutomobila)
            recyclerView.layoutManager = GridLayoutManager(this, 2)
        }

        dugmeNazad.setOnClickListener {
            recyclerView.adapter = MojAdapterLogotip(listaAutomobila)
            recyclerView.layoutManager = GridLayoutManager(this, 2)
            pretraga.setText("")
        }
    }

    private fun createDataSource() {
        listaAutomobila.add(Logotipi(R.drawable.alfa_romeo, "Alfa Romeo"))
        listaAutomobila.add(Logotipi(R.drawable.audi, "Audi"))
        listaAutomobila.add(Logotipi(R.drawable.citroen, "Citroen"))
        listaAutomobila.add(Logotipi(R.drawable.dacia, "Dacia"))
        listaAutomobila.add(Logotipi(R.drawable.fiat, "Fiat"))
        listaAutomobila.add(Logotipi(R.drawable.ford, "Ford"))
        listaAutomobila.add(Logotipi(R.drawable.honda, "Honda"))
        listaAutomobila.add(Logotipi(R.drawable.hyndai, "Hyundai"))
        listaAutomobila.add(Logotipi(R.drawable.infiniti, "Infiniti"))
        listaAutomobila.add(Logotipi(R.drawable.isuzu, "Isuzu"))
        listaAutomobila.add(Logotipi(R.drawable.jeep, "Jeep"))
        listaAutomobila.add(Logotipi(R.drawable.lada, "Lada"))
        listaAutomobila.add(Logotipi(R.drawable.mazda, "Mazda"))
        listaAutomobila.add(Logotipi(R.drawable.mercedes_benz, "Mercedes-Benz"))
        listaAutomobila.add(Logotipi(R.drawable.mini, "Mini"))
        listaAutomobila.add(Logotipi(R.drawable.mitsubishi, "Mitsubishi"))
        listaAutomobila.add(Logotipi(R.drawable.nissan, "Nissan"))
        listaAutomobila.add(Logotipi(R.drawable.opel, "Opel"))
        listaAutomobila.add(Logotipi(R.drawable.peugeot, "Peugeot"))
        listaAutomobila.add(Logotipi(R.drawable.renault, "Renault"))
        listaAutomobila.add(Logotipi(R.drawable.seat, "Seat"))
        listaAutomobila.add(Logotipi(R.drawable.skoda, "Skoda"))
        listaAutomobila.add(Logotipi(R.drawable.smart, "Smart"))
        listaAutomobila.add(Logotipi(R.drawable.subaru, "Subaru"))
        listaAutomobila.add(Logotipi(R.drawable.suzuki, "Suzuki"))
        listaAutomobila.add(Logotipi(R.drawable.volkswagen, "Volkswagen"))
        listaAutomobila.add(Logotipi(R.drawable.volvo, "Volvo"))
    }
}