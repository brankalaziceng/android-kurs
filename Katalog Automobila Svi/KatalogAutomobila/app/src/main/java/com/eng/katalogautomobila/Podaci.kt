package com.eng.katalogautomobila

object DataGlobal {
    var cars: ArrayList<Car> = arrayListOf()

    var carModel = ""

    var carLogo: Int = 0

    data class CarImage(val image: Int, val car: String)

    data class OdabraniAutomobili(val slika: Int, val model: String, val cena: String, val marka: String,
                              val motor: String, val paketOpreme: String, val tezinaPraznogVozila: String)

    fun getPictureOfACar(marka: String): Int {
        if (marka == "Alfa Romeo") {
            return R.drawable.alfa_romeo_giulietta
        } else if (marka.contains("Audi")) {
            return R.drawable.audi_picture
        } else if (marka == "Citroen") {
            return R.drawable.citroen_picture
        } else if (marka == "Dacia") {
            return R.drawable.dacia_white
        } else if (marka == "Fiat") {
            return R.drawable.fiat_500_l
        } else if (marka == "Ford") {
            return R.drawable.ford_fiesta
        } else if (marka == "Honda") {
            return R.drawable.honda_orange
        } else if (marka == "Hyundai") {
            return R.drawable.hyundai_blue
        } else if (marka == "Infiniti") {
            return R.drawable.infiniti_automobil
        } else if (marka == "Isuzu") {
            return R.drawable.isuzu_sivi
        } else if (marka == "Jeep") {
            return R.drawable.jeep_beli
        }
        return R.drawable.automobil
    }
}
