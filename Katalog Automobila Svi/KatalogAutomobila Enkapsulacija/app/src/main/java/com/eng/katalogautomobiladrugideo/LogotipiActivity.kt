package com.eng.katalogautomobiladrugideo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_logotipi.*

var rowIndex: Int? = null

data class Logotipi(val slika: Int, val naziv: String)

class LogotipiActivity : AppCompatActivity() {

    val listaAutomobila: ArrayList<Logotipi> = arrayListOf()

    var listaPretrazenihAutomobila: ArrayList<DataGlobal.OdabraniAutomobili> = arrayListOf()

    val uvezenaKlasa = NovaKlasa()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logotipi)

        uvezenaKlasa.createDataSource(listaAutomobila)

        val kreiraniAdapter = MojAdapterLogotip(listaAutomobila)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))

        dugmePretraga.setOnClickListener {
            listaPretrazenihAutomobila.clear()

            for (car in DataGlobal.cars) {
                //val unos = (car.model).toLowerCase()
                val unos = (car.marka + " " + car.model).toLowerCase()

                if (pretraga.text.toString().isNotEmpty() && unos.contains(
                        pretraga.text.toString().toLowerCase())) {
                    val slika = uvezenaKlasa.getPictureOfACar(car.marka)
                    listaPretrazenihAutomobila.add(
                        DataGlobal.OdabraniAutomobili(
                            uvezenaKlasa.getPictureOfACar(car.marka), car.model, car.cena + " $", car.marka, car.motor,
                            car.paketOpreme, car.tezinaPraznogVozila))
                    Log.d("image", car.marka)
                }
            }
            recyclerView.adapter = MojAdapterModel(listaPretrazenihAutomobila)
            recyclerView.layoutManager = GridLayoutManager(this, 2)
        }

        dugmeNazad.setOnClickListener {
            recyclerView.adapter = MojAdapterLogotip(listaAutomobila)
            recyclerView.layoutManager = GridLayoutManager(this, 2)
            pretraga.setText("")
        }
    }
}