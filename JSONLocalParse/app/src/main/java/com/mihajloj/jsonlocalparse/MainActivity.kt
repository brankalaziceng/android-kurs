package com.mihajloj.jsonlocalparse

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

class MainActivity : AppCompatActivity() {

    val studenti: ArrayList<Student> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        procitajJSON()

        for (i in studenti){
            Log.d("STU", i.ime)
        }
    }

    fun procitajJSON(){

        val json : String?

        try {
            val InputStream: InputStream = assets.open("localJSON.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }
            val ispis = findViewById<TextView>(R.id.ispis)
            var ovoCuIspisati = ""
            val jsonniz = JSONArray(json)

            jsonniz.let {
                (0 until it.length()).forEach{
                    val jsonObjekat = jsonniz.getJSONObject(it)

                    val ime = jsonObjekat.getString("ime")
                    val prezime = jsonObjekat.getString("prezime")
                    val mesto = jsonObjekat.getString("mesto")
                    val godiste = jsonObjekat.getInt("godiste")
                    val visina = jsonObjekat.getInt("visina")
                    val prosek = jsonObjekat.getDouble("prosek")

                    studenti.add(Student(ime,prezime, mesto,godiste,visina,prosek))
                    ovoCuIspisati = ovoCuIspisati + ime + " " + prezime + " " + mesto + " " + godiste.toString() + " " + visina.toString() + " " + prosek.toString() + "\n"

                    val student = jsonObjekat.getJSONArray("student")
                    student.let {
                        (0 until it.length()).forEach {
                            val studentIzNiza = student.getJSONObject(it)

                            val ime1 = studentIzNiza.getString("ime")
                            val prezime1 = studentIzNiza.getString("prezime")
                            val mesto1 = studentIzNiza.getString("mesto")
                            val godiste1 = studentIzNiza.getInt("godiste")
                            val visina1 = studentIzNiza.getInt("visina")
                            val prosek1 = studentIzNiza.getDouble("prosek")

                            ovoCuIspisati = ovoCuIspisati + ime1 + " " + prezime1 + " " + mesto1 + " " + godiste1.toString() + " " + visina1.toString() + " " + prosek1.toString() + "\n"

                        }
                    }
                }


            }

//            val jsonObjekat = JSONObject(json)
//
//            val ime = jsonObjekat.getString("ime")
//            val prezime = jsonObjekat.getString("prezime")
//            val mesto = jsonObjekat.getString("mesto")
//            val godiste = jsonObjekat.getInt("godiste")
//            val visina = jsonObjekat.getInt("visina")
//            val prosek = jsonObjekat.getDouble("prosek")


//            val ovoCuIspisati = ime.toString() + " " + prezime + " " + mesto + " " + godiste + " " + visina + " " + prosek
            ispis.text = ovoCuIspisati

        }
        catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

    }

}

class Student(
    val ime: String,
    val prezime: String,
    val mesto: String,
    val godiste: Int,
    val visina: Int,
    val prosek: Double
)