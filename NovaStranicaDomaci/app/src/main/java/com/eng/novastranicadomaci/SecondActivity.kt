package com.eng.novastranicadomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val mainActivity = findViewById<Button>(R.id.mainActivity)
        val firstActivity = findViewById<Button>(R.id.firstActivity)
        val thirdActivity = findViewById<Button>(R.id.thirdActivity)

        mainActivity.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        firstActivity.setOnClickListener {
            startActivity(Intent(this, FirstActivity::class.java))
            finish()
        }

        thirdActivity.setOnClickListener {
            startActivity(Intent(this, ThirdActivity::class.java))
            finish()
        }
    }
}