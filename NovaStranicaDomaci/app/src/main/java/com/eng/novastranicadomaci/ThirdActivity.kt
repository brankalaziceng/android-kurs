package com.eng.novastranicadomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ThirdActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        val mainActivity = findViewById<Button>(R.id.mainActivity)
        val firstActivity = findViewById<Button>(R.id.firstActivity)
        val secondActivity = findViewById<Button>(R.id.secondActivity)

        mainActivity.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        firstActivity.setOnClickListener {
            startActivity(Intent(this, FirstActivity::class.java))
            finish()
        }

        secondActivity.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
            finish()
        }
    }
}