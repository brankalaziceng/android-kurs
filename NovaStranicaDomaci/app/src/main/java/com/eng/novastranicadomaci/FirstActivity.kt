package com.eng.novastranicadomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class FirstActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)

        val mainActivity = findViewById<Button>(R.id.mainActivity)
        val secondActivity = findViewById<Button>(R.id.secondActivity)
        val thirdActivity = findViewById<Button>(R.id.thirdActivity)

        mainActivity.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        secondActivity.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
            finish()
        }

        thirdActivity.setOnClickListener {
            startActivity(Intent(this, ThirdActivity::class.java))
            finish()
        }
    }
}