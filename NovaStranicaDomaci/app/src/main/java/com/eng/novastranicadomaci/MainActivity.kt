package com.eng.novastranicadomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firstActivity = findViewById<Button>(R.id.firstActivity)
        val secondActivity = findViewById<Button>(R.id.secondActivity)
        val thirdActivity = findViewById<Button>(R.id.thirdActivity)

        firstActivity.setOnClickListener {
            startActivity(Intent(this, FirstActivity::class.java))
            finish()
        }

        secondActivity.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
            finish()
        }

        thirdActivity.setOnClickListener {
            startActivity(Intent(this, ThirdActivity::class.java))
            finish()
        }
    }
}