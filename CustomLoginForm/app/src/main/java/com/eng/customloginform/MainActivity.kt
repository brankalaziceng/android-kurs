package com.eng.customloginform

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var otvoren: Boolean = false

        var clicked: Boolean = false
        var clickedGreen: Boolean = false
        var clickedRed: Boolean = false

        zatvori_link.setOnClickListener{
            if(!otvoren) {
                padajuci_meni_zatvoren.visibility = View.VISIBLE
                zatvori_link.text = "- Zatvori padajuci meni"
                println(otvoren)
            } else {
                padajuci_meni_zatvoren.visibility = View.GONE
                zatvori_link.text = "- Otvori padajuci meni"
                println(otvoren)
            }
            otvoren = !otvoren
        }

        zameni_u_plavo.setOnClickListener {
                if(!clicked){
                    zameni_u_plavo.setBackgroundResource(R.color.plavo)
                    clicked = false

                }else{
                    zameni_u_plavo.setBackgroundResource(R.drawable.crveniokvir)
                    clicked = true
                }
                clicked = !clicked
            }
            //zamjeni_u_plavu.setBackgroundColor(Color.parseColor("#0000FF"))
            //zameni_u_plavo.setBackgroundResource(R.color.plavo)

        zameni_u_zeleno.setOnClickListener {
            if(!clickedGreen){
                zameni_u_zeleno.setBackgroundResource(R.color.zelena)
                clickedGreen = false

            }else{
                zameni_u_zeleno.setBackgroundResource(R.drawable.crveniokvir)
                clickedGreen = true
            }
            clickedGreen = !clickedGreen
        }

        zameni_u_crveno.setOnClickListener {
            if(!clickedRed){
                zameni_u_crveno.setBackgroundResource(R.color.red)
                clickedRed = false

            }else{
                zameni_u_crveno.setBackgroundResource(R.drawable.crveniokvir)
                clickedRed = true
            }
            clickedRed = !clickedRed
        }
        }
}