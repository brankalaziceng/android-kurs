package com.eng.countrieslayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var open = false

        show_more.setOnClickListener {
            if (!open) {
                scroll_down_zatvoren.visibility = View.VISIBLE
                scroll.text = "Scroll Up"
            } else {
                scroll_down_zatvoren.visibility = View.GONE
                scroll.text = "Scroll Down"
            }
            open = !open
        }
    }
}