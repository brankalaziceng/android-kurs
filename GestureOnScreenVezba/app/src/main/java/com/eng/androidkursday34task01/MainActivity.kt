package com.eng.androidkursday34task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import kotlin.math.abs

class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {

    lateinit var gestureDetector: GestureDetector
    lateinit var imageView: ImageView
    var nizSlika: ArrayList<Int> = arrayListOf()
    val MIN_DISTANCA = 150
    var x1: Float = 0.0f
    var x2: Float = 0.0f
    var y1: Float = 0.0f
    var y2: Float = 0.0f
    var brojacBoje = 0
    var brojacSlika = 0
    var nizBoja: ArrayList<Int> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gestureDetector = GestureDetector(this, this)

        nizBoja.add(R.color.red)
        nizBoja.add(R.color.green)
        nizBoja.add(R.color.blue)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event!!.action) {
            0 -> {
                x1 = event.x
                y1 = event.y
            }

            1 -> {
                x2 = event.x
                y2 = event.y

                val vrednostX: Float = x2 - x1
                val vrednostY: Float = y2 - y1
                if (abs(vrednostY) > MIN_DISTANCA) {
                    if (y2 > y1) {  // Swipe gore
                        zaustaviBoju = true
                        nizSlika.add(R.drawable.image1)
                        nizSlika.add(R.drawable.image2)
                        nizSlika.add(R.drawable.image3)
                        nizSlika.add(R.drawable.image4)
                        imageView.setImageResource(nizSlika[0])
                    } else {
                        zaustaviBoju = false
                        prikaziBoju()
                    }
                } else if (abs(vrednostX) > MIN_DISTANCA) {
                    if (x2 > x1) {  // Swipe levo
                        zaustaviBoju = true
                        if (brojacSlika > 2)
                            brojacSlika = -1
                        imageView.setImageResource(nizSlika[++brojacSlika])
                    } else {
                        zaustaviBoju = true
                        if (brojacSlika < 1)
                            brojacSlika = 4
                        imageView.setImageResource(nizSlika[--brojacSlika])
                    }
                }
            }
        }

        return gestureDetector.onTouchEvent(event)
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }

    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent?,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) {
        val layout = findViewById<LinearLayout>(R.id.layoutLinearLayout)
        imageView = ImageView(this)
        imageView.layoutParams = LinearLayout.LayoutParams(1500, 950)
        imageView.setBackgroundColor(resources.getColor(nizBoja[0]))
        layout.addView(imageView)
    }

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        return false
    }

    var zaustaviBoju = false

    fun prikaziBoju() {
        if (!zaustaviBoju) {
            Handler(Looper.getMainLooper()).postDelayed({
                imageView.setImageResource(menjajBoju())
                prikaziBoju()
            }, 500)
        }
    }

    fun menjajBoju() : Int {
        if (brojacBoje > 2)
            brojacBoje = 0
        return nizBoja[brojacBoje++]
    }
}